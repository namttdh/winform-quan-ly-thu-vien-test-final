﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManager.DTO
{
    class DTO_Book
    {
        public int id { get; set; }
        public string name { get; set; }
        public string describe { get; set; }
        public string nameAuthor { get; set; }
        public int amount { get; set; }
        public DTO_Book()
        {

        }
        public DTO_Book(int _id, string _name, string _describe, string _nameAuthor, int _amount)
        {
            id = _id;
            name = _name;
            describe = _describe;
            nameAuthor = _nameAuthor;
            amount = _amount;
        }
    }
}
