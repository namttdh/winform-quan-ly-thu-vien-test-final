﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quanlythuvien2.DTO
{
    class DTO_BookOrder
    {
        public int id;
        public int idUser;
        public long date;
        public long dateReturn;
        public long datePromise;
        public int amount;
    }
}
