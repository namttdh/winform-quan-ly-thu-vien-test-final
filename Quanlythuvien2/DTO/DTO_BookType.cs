﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManager.DTO
{
    class DTO_BookType
    {
        public DTO_BookType()
        {

        }
        public DTO_BookType(string _name, string _note = "")
        {
            name = _name;
            note = _note;
        }
        public int id { get; set; }
        public string name { get; set; }
        public string note { get; set; }

        //public bool IsChoose { get; set; }

        
        public bool IsChoose { get; set; } 
        

    }
}
