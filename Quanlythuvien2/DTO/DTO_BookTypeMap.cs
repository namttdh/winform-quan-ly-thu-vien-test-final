﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManager.DTO
{
    class DTO_BookTypeMap 
    {
        public int id { get; set; }
        public int idBook { get; set; }
        public int idType { get; set; }
        public DTO_BookTypeMap()
        {

        }
        public DTO_BookTypeMap(int _id, int _idBook, int _idType)
        {
            id = _id;
            idBook = _idBook;
            idType = _idType;
        }
    }
}
