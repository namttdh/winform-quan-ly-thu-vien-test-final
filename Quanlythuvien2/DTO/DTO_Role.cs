﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quanlythuvien2.DTO
{
    public class DTO_Role
    {
        public int id;
        public string name { get; set; }
        public string note { get; set; }
        public DTO_Role()
        {

        }
        public DTO_Role(string _name, string _note)
        {
            name = _name;
            note = _note;
        }
    }
}
