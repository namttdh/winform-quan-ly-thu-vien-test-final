﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quanlythuvien2.GUI
{
    class DTO_RoleMap
    {
        public int id;
        public int idUser { get; set; }
        public int idRole { get; set; }
        public DTO_RoleMap()
        {

        }
        public DTO_RoleMap(int _id, int _idRole)
        {
            idUser = _id;
            idRole = _idRole;
        }
    }
}
