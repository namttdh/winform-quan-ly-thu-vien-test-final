﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserAndUserGroup.DTO
{
    public class DTO_User
    {
        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public DTO_User()
        {

        }
        public DTO_User(string _username, string _password, string _name, string _address)
        {
            username = _username;
            password = _password;
            name = _name;
            address = _address;
        }
    }
}
