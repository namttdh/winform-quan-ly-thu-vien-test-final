﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserAndUserGroup.DTO
{
    class DTO_UserGroup
    {
        public DTO_UserGroup()
        {

        }
        public DTO_UserGroup(string _name, string _note = "")
        {
            name = _name;
            note = _note;
        }
        public int id { get; set; }
        public string name { get; set; }
        public string note { get; set; }

    }
}
