﻿using LibraryManager.DTO;
using LibraryManager.DTO.Opject;
using Quanlythuvien.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManager.DataAccess
{
    class DAL_Book : Database
    {
        private int id;
        private string name;
        private string describe;
        private string nameAuthor;
        private int amount;
        public int AddBook(DTO_Book newBook)
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string _SQLCommand = string.Format("INSERT INTO Book(name,describe,nameAuthor,amount) VALUES (N'{0}',N'{1}',N'{2}',{3}); SELECT SCOPE_IDENTITY()", newBook.name, newBook.describe, newBook.nameAuthor, newBook.amount);
            SqlCommand sqlCmd = new SqlCommand(_SQLCommand, _SQLConnection);
            int insertedID = Convert.ToInt32(sqlCmd.ExecuteScalar());
            _SQLConnection.Close();
            return insertedID;
        }
        public int AddBookTypeMap(int idBook, int idType)
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string _SQLCommand = string.Format("INSERT INTO BookTypeMap(idBook,idType) VALUES ({0},{1})", idBook, idType);
            SqlCommand sqlCmd = new SqlCommand(_SQLCommand, _SQLConnection);
            int insertedID = Convert.ToInt32(sqlCmd.ExecuteScalar());
            _SQLConnection.Close();
            return insertedID;
        }
        public int AddBookGroupMap(int idBook, int idGroup)
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string _SQLCommand = string.Format("INSERT INTO BookGroupMap(idBook,idGroup) VALUES ({0},{1})", idBook, idGroup);
            SqlCommand sqlCmd = new SqlCommand(_SQLCommand, _SQLConnection);
            int insertedID = Convert.ToInt32(sqlCmd.ExecuteScalar());
            _SQLConnection.Close();
            return insertedID;
        }

        public bool DeleteBookTypeMap(int id)
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string _SQLCommand = string.Format("DELETE FROM BookTypeMap WHERE idBook={0}", id);
            SqlCommand sqlCmd = new SqlCommand(_SQLCommand, _SQLConnection);
            sqlCmd.ExecuteNonQuery();
            _SQLConnection.Close();
            return true;
        }
        public bool DeleteBookGroupMap(int id)
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string _SQLCommand = string.Format("DELETE FROM BookGroupMap WHERE idBook={0}", id);
            SqlCommand sqlCmd = new SqlCommand(_SQLCommand, _SQLConnection);
            sqlCmd.ExecuteNonQuery();
            _SQLConnection.Close();
            return true;
        }
        public bool DeleteBook(int id)
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string _SQLCommand = string.Format("DELETE FROM Book WHERE id={0}", id);
            SqlCommand sqlCmd = new SqlCommand(_SQLCommand, _SQLConnection);
            sqlCmd.ExecuteNonQuery();
            _SQLConnection.Close();
            return true;
        }

        public void UpdateBook(DTO_Book updateBook)
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string _SQLCommand = string.Format("UPDATE Book SET name=N'{0}', describe=N'{1}',nameAuthor = N'{2}',amount = N'{3}' WHERE id={4}", updateBook.name, updateBook.describe, updateBook.nameAuthor,updateBook.amount, updateBook.id);
            SqlCommand sqlCmd = new SqlCommand(_SQLCommand, _SQLConnection);
            sqlCmd.ExecuteNonQuery();
            _SQLConnection.Close();
        }

        public List<DTO_Book> SearchBookByName(string strKeyword)
        {
            _SQLConnection.Open();
            string _sqlCommand = string.Format("SELECT * FROM Book where  name like N'%" + strKeyword + "%' ");
            List<DTO_Book> listAllBook = convertToDTOBook(new SqlCommand(_sqlCommand, _SQLConnection).ExecuteReader());
            _SQLConnection.Close();
            return listAllBook;
        }

        public List<BookTypeMap> getAllBook(string dk = "")
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string _sqlCommand = string.Format("" +
                "SELECT * FROM (SELECT DISTINCT b.*, SUBSTRING((SELECT ', '+bt.name  AS [text()] FROM BookTypeMap as btm INNER JOIN BookType as bt on bt.id = btm.idType WHERE b.id = btm.idBook FOR XML PATH ('') ), 2, 1000) [nameType]," +
                "SUBSTRING((SELECT ', '+bg.name  AS [text()] FROM BookGroupMap as bgm INNER JOIN BookGroup as bg on bg.id = bgm.idGroup WHERE b.id = bgm.idBook FOR XML PATH ('')), 2, 1000) [nameGroup]" +
                " FROM Book as b)[bk] " + dk);
            List<BookTypeMap> listAllBook = convertToBook(new SqlCommand(_sqlCommand, _SQLConnection).ExecuteReader());
            _SQLConnection.Close();
            return listAllBook;
        }
        List<BookTypeMap> convertToBook(SqlDataReader result)
        {
            List<BookTypeMap> listAllBook = new List<BookTypeMap>();
            while (result.Read())
            {
                BookTypeMap Book = new BookTypeMap();
                Book.id = Convert.ToInt32(result["id"]);
                Book.name = result["name"].ToString();
                Book.describe = result["describe"].ToString();
                Book.nameAuthor = result["nameAuthor"].ToString();
                Book.amount = Convert.ToInt32(result["amount"]);
                Book.nameType = result["nameType"].ToString();
                Book.nameGroup = result["nameGroup"].ToString();
                listAllBook.Add(Book);
            }
            return listAllBook;
        }
        List<DTO_Book> convertToDTOBook(SqlDataReader result)
        {
            List<DTO_Book> listAllBook = new List<DTO_Book>();
            while (result.Read())
            {
                DTO_Book Book = new DTO_Book();
                Book.id = Convert.ToInt32(result["id"]);
                Book.name = result["name"].ToString();
                Book.describe = result["describe"].ToString();
                Book.nameAuthor = result["nameAuthor"].ToString();
                Book.amount = Convert.ToInt32(result["amount"]);
                listAllBook.Add(Book);
            }
            return listAllBook;
        }
        public BookTypeMap GetBookByID(int id)
        {
            BookTypeMap ketqua = null;
            _SQLConnection.Open();
            string _sqlCommand = string.Format("SELECT * FROM Book where  id =" + id + "");
            SqlCommand sqlCmd = new SqlCommand(_sqlCommand, _SQLConnection);
            SqlDataReader result;
            result = sqlCmd.ExecuteReader();
            while (result.Read())
            {
                ketqua = new BookTypeMap();
                ketqua.id = Convert.ToInt32(result["id"]);
                ketqua.name = result["name"].ToString();
                ketqua.describe = result["describe"].ToString();
                ketqua.nameAuthor = result["nameAuthor"].ToString();
                ketqua.amount = Convert.ToInt32(result["amount"]);
            }
            _SQLConnection.Close();
            return ketqua;
        }
        public List<DTO_Book> findBookByIdOrder(string idOrder)
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string _sqlCommand = string.Format("SELECT Book. *from Book INNER JOIN BookOrderMap bom on Book.id = bom.idBook WHERE idOrder = {0}", idOrder);
            SqlCommand sqlCmd = new SqlCommand(_sqlCommand, _SQLConnection);
            List<DTO_Book> allOrder = convertToDTOBook(sqlCmd.ExecuteReader());
            _SQLConnection.Close();
            return allOrder;
            
        }
    }
}
