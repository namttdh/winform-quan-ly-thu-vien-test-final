﻿using LibraryManager.DTO;
using Quanlythuvien.Core;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManager.DataAccess
{
    class DAL_BookGroup : Database
    {
        private int id;
        private string name;
        private string note;

        public bool AddBookGroup(DTO_BookGroup newGroup)
        {
            _SQLConnection.Open();
            string _sqlcommand = string.Format("insert into BookGroup(name,note) values (N'{0}',N'{1}')", newGroup.name, newGroup.note);
            SqlCommand sqlcmd = new SqlCommand(_sqlcommand, _SQLConnection);
            if (sqlcmd.ExecuteNonQuery() > 0)
            {
                return true;
            }
            _SQLConnection.Close();
            return false;
        }
       
        public bool DeleteBookGroup(int id)
        {
            _SQLConnection.Open();
            string _SQLCommand = string.Format("DELETE FROM BookGroup WHERE id={0}", id);
            SqlCommand sqlCmd = new SqlCommand(_SQLCommand, _SQLConnection);
            if (sqlCmd.ExecuteNonQuery() > 0)
            {
                return true;
            }
            _SQLConnection.Close();
            return false;
        }
        public void UpdateBookGroup(DTO_BookGroup updateGroup)
        {
            _SQLConnection.Open();
            string _SQLCommand = string.Format("UPDATE BookGroup SET name=N'{0}', note=N'{1}' WHERE id={2}", updateGroup.name, updateGroup.note, updateGroup.id);
            SqlCommand sqlcmd = new SqlCommand(_SQLCommand, _SQLConnection);
            sqlcmd.ExecuteNonQuery();
            _SQLConnection.Close();
        }

        public List<DTO_BookGroup> getAllGroupBook()
        {
            _SQLConnection.Open();
            string _sqlCommand = string.Format("SELECT * FROM BookGroup");
            List<DTO_BookGroup> listAllGroup = convertToBookGroup(new SqlCommand(_sqlCommand, _SQLConnection).ExecuteReader());
            _SQLConnection.Close();
            return listAllGroup;
        }

        public List<DTO_BookGroup> SearchBookGroupByName(string strKeyword)
        {
            _SQLConnection.Open();
            string _sqlCommand = string.Format("SELECT * FROM BookGroup where  name like N'%" + strKeyword + "%' ");
            List<DTO_BookGroup> listAllGroup = convertToBookGroup(new SqlCommand(_sqlCommand, _SQLConnection).ExecuteReader());
            _SQLConnection.Close();
            return listAllGroup;
        }
        List<DTO_BookGroup> convertToBookGroup(SqlDataReader result)
        {
            List<DTO_BookGroup> listAllGroup = new List<DTO_BookGroup>();
            while (result.Read())
            {
                DTO_BookGroup bookGroup = new DTO_BookGroup();
                bookGroup.id = Convert.ToInt32(result["id"]);
                bookGroup.name = result["name"].ToString();
                bookGroup.note = result["note"].ToString();
                listAllGroup.Add(bookGroup);
            }
            return listAllGroup;
        }

        public DTO_BookGroup GetBookGroupByID(int id)
        {
            DTO_BookGroup ketqua = null;
            _SQLConnection.Open();
            string _sqlCommand = string.Format("SELECT * FROM BookGroup where  ID =" + id + "");
            SqlCommand sqlCmd = new SqlCommand(_sqlCommand, _SQLConnection);
            SqlDataReader result;
            result = sqlCmd.ExecuteReader();
            while (result.Read())
            {
                ketqua = new DTO_BookGroup();
                ketqua.id = Convert.ToInt32(result["id"]);
                ketqua.name = result["name"].ToString();
                ketqua.note = result["note"].ToString();

            }
            _SQLConnection.Close();
            return ketqua;
        }
    }
}

