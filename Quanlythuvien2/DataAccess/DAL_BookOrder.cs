﻿using Quanlythuvien.Core;
using Quanlythuvien2.Core;
using Quanlythuvien2.DTO;
using Quanlythuvien2.Object;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quanlythuvien2.DataAccess
{
    class DAL_BookOrder:Database
    {
        public List<BookOrderWithName> getAllBookOrder(bool desc = false)
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string _sqlCommand = string.Format("Select bo.*, name from BookOrder as bo Inner join [User] on bo.idUser = [User].id ");
            _sqlCommand += desc ? " Order by id DESC" : "";
            SqlCommand sqlCmd = new SqlCommand(_sqlCommand, _SQLConnection);
            List<BookOrderWithName> allOrder = convertToList(sqlCmd.ExecuteReader());
            _SQLConnection.Close();
            return allOrder;
        }
        public List<BookOrderWithName> findById(string id)
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string _sqlCommand = string.Format("Select bo.*, name from BookOrder as bo Inner join [User] on bo.idUser = [User].id WHERE bo.id = {0}", id);
            SqlCommand sqlCmd = new SqlCommand(_sqlCommand, _SQLConnection);
            List<BookOrderWithName> allOrder = convertToList(sqlCmd.ExecuteReader());
            _SQLConnection.Close();
            return allOrder;
        }
        public List<BookOrderWithName> getBookOrderByTimeAndMaybeName(long timeStart, long timeEnd, string name = "",  bool expried = false, bool desc = false)
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string _sqlCommand = string.Format("Select bo.*, name from BookOrder as bo Inner join [User] on bo.idUser = [User].id where date >= {0} and date <= {1}",timeStart,timeEnd);
            _sqlCommand += expried ? " and dateReturn is NULL and (datePromise - "+Helper.ConvertToUnixTime(DateTime.Now)+") <= 0 ":"";
            _sqlCommand += name != "" ? " and name like N'%" + name + "%' " : "";
            _sqlCommand += desc ? " Order by id DESC" : "";
            SqlCommand sqlCmd = new SqlCommand(_sqlCommand, _SQLConnection);
            List<BookOrderWithName> allOrder = convertToList(sqlCmd.ExecuteReader());
            _SQLConnection.Close();
            return allOrder;
        }

        public void deleteBookOrderMap(string[] listId)
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string id = "";
            foreach (string idT in listId)
            {
                id += idT + ",";
            }
            id = id.Remove(id.Length - 1, 1);
            string _sqlCommand = string.Format("DELETE FROM BookOrderMap WHERE idOrder in ({0})", id);
            var sqlCmd = new SqlCommand(_sqlCommand, _SQLConnection);
            sqlCmd.ExecuteNonQuery();
            _SQLConnection.Close();
        }
        public bool deleteByListId(string[] listId)
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string id = "";
            foreach(string idT in listId)
            {
                id += idT + ",";                
            }
            id = id.Remove(id.Length - 1, 1);
            string _sqlCommand = string.Format("DELETE FROM BookOrder WHERE id in ({0})", id);
            var sqlCmd = new SqlCommand(_sqlCommand, _SQLConnection);
            sqlCmd.ExecuteNonQuery();
            _SQLConnection.Close();
            return true;
        }
        public void updateOrder(DTO_BookOrder bookOrder, int amount)
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string _sqlCommand = string.Format("UPDATE BookOrder set idUser = {0}, datePromise = {1}, amount = {2} WHERE id ={3}", bookOrder.idUser, bookOrder.datePromise, amount, bookOrder.id);
            SqlCommand sqlCmd = new SqlCommand(_sqlCommand, _SQLConnection);
            sqlCmd.ExecuteNonQuery();
            _SQLConnection.Close();
        }
        public void updateDateReturn(DateTime dateTime, string[] listId)
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string id = "";
            foreach (string idT in listId)
            {
                id += idT + ",";
            }
            id = id.Remove(id.Length - 1, 1);

            string _sqlCommand = string.Format("UPDATE BookOrder set dateReturn = {0} WHERE id in ({1})", Helper.ConvertToUnixTime(dateTime), id);            
            SqlCommand sqlCmd = new SqlCommand(_sqlCommand, _SQLConnection);
            sqlCmd.ExecuteNonQuery();
            _SQLConnection.Close();
        }
        public void updateDateNotReturn(string[] listId)
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string id = "";
            foreach (string idT in listId)
            {
                id += idT + ",";
            }
            id = id.Remove(id.Length - 1, 1);
            string _sqlCommand =  string.Format("UPDATE BookOrder set dateReturn = null WHERE id in ({0})", id);            
            SqlCommand sqlCmd = new SqlCommand(_sqlCommand, _SQLConnection);
            sqlCmd.ExecuteNonQuery();
            _SQLConnection.Close();
        }
        public bool deleteOrderMap(int idOrder, List<int> listBook)
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string _sqlCommand = "";
            foreach (int idBook in listBook)
            {
                _sqlCommand += String.Format("DELETE FROM BookOrderMap WHERE idOrder = {0} and idBook = {1} ", idOrder, idBook);
            }
            SqlCommand sqlCmd = new SqlCommand(_sqlCommand, _SQLConnection);
            sqlCmd.ExecuteNonQuery();
            _SQLConnection.Close();
            return true;
        }
        public int insertOrder(DTO_BookOrder bookOrder, int amount)
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string _sqlCommand = string.Format("INSERT INTO BookOrder(idUser,date,datePromise,amount) OUTPUT Inserted.ID VALUES ({0},{1},{2},{3})", bookOrder.idUser, bookOrder.date, bookOrder.datePromise, amount);
            SqlCommand sqlCmd = new SqlCommand(_sqlCommand, _SQLConnection);
            SqlDataReader result = sqlCmd.ExecuteReader();
            int idBookOrder = 0;
            while (result.Read())
            {
                idBookOrder = Convert.ToInt32(result["ID"]);
            }
            _SQLConnection.Close();
            return idBookOrder;
        }
        public bool insertOrderMap(int idOrder, List<int> listBook)
        {
            if (_SQLConnection.State != ConnectionState.Open) _SQLConnection.Open();
            string _sqlCommand = "";
            foreach (int idBook in listBook)
            {
                _sqlCommand += String.Format("INSERT INTO BookOrderMap VALUES ({0},{1})", idOrder, idBook);
            }
            SqlCommand sqlCmd = new SqlCommand(_sqlCommand, _SQLConnection);
            sqlCmd.ExecuteNonQuery();
            _SQLConnection.Close();
            return true;
        }
        public List<BookOrderWithName> convertToList(SqlDataReader result)
        {
            List<BookOrderWithName> allOrder = new List<BookOrderWithName>();
            while (result.Read())
            {
                BookOrderWithName bookOrder = new BookOrderWithName();
                bookOrder.id = Convert.ToInt32(result["id"]);
                bookOrder.idUser = Convert.ToInt32(result["idUser"]);
                bookOrder.date = Convert.ToInt64(result["date"]);
                bookOrder.datePromise = Convert.ToInt64(result["datePromise"]);
                bookOrder.dateReturn = (DBNull.Value.Equals(result["dateReturn"])) ? 0 : Convert.ToInt64(result["dateReturn"]);
                bookOrder.amount = Convert.ToInt32(result["amount"]);
                bookOrder.name = result["name"].ToString();
                allOrder.Add(bookOrder);
            }
            return allOrder;
        }
    }
}
