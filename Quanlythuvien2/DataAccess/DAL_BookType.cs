﻿using LibraryManager.DTO;
using Quanlythuvien.Core;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManager.DataAccess
{
    class DAL_BookType : Database
    {
        public bool AddBookType(DTO_BookType newType)
        {
            _SQLConnection.Open();
            string _SQLCommand = string.Format("INSERT INTO BookType(name,note) VALUES (N'{0}',N'{1}')", newType.name, newType.note);
            SqlCommand sqlCmd = new SqlCommand(_SQLCommand, _SQLConnection);
            if (sqlCmd.ExecuteNonQuery() > 0)
            {
                return true;
            }
            _SQLConnection.Close();
            return false;
        }
        public bool DeleteBookType(int id)
        {
            _SQLConnection.Open();
            string _SQLCommand = string.Format("DELETE FROM BookType WHERE id={0}", id);
            SqlCommand sqlCmd = new SqlCommand(_SQLCommand, _SQLConnection);
            if (sqlCmd.ExecuteNonQuery() > 0)
            {
                return true;
            }
            _SQLConnection.Close();
            return false;
        }
        
        public void UpdateBookType(DTO_BookType updateType)
        {
            _SQLConnection.Open();
            string _SQLCommand = string.Format("UPDATE BookType SET name=N'{0}', note=N'{1}' WHERE id={2}", updateType.name, updateType.note, updateType.id);
            SqlCommand sqlcmd = new SqlCommand(_SQLCommand, _SQLConnection);
            sqlcmd.ExecuteNonQuery();
            _SQLConnection.Close();
        }

        public List<DTO_BookType> getAllTypeBook(string dk = "")
        {
            _SQLConnection.Open();
            string _sqlCommand = string.Format("SELECT * FROM BookType "+dk);
            List<DTO_BookType> listAllType = convertToBookType(new SqlCommand(_sqlCommand, _SQLConnection).ExecuteReader());
            _SQLConnection.Close();
            return listAllType;
        }
        
        public List<DTO_BookType> SearchBookTypeByName(string strKeyword)
        {
            _SQLConnection.Open();
            string _sqlCommand = string.Format("SELECT * FROM BookType where  name like N'%" + strKeyword + "%' ");
            List<DTO_BookType> listAllType = convertToBookType(new SqlCommand(_sqlCommand, _SQLConnection).ExecuteReader());
            _SQLConnection.Close();
            return listAllType;            
        }
        List<DTO_BookType> convertToBookType(SqlDataReader result)
        {
            List<DTO_BookType> listAllType = new List<DTO_BookType>();
            while (result.Read())
            {
                DTO_BookType bookType = new DTO_BookType();
                bookType.id = Convert.ToInt32(result["id"]);
                bookType.name = result["name"].ToString();
                bookType.note = result["note"].ToString();
                listAllType.Add(bookType);
            }
            return listAllType;
        }
        public DTO_BookType GetBookTypeByID(int id)
        {
            DTO_BookType ketqua = null;
            _SQLConnection.Open();
            string _sqlCommand = string.Format("SELECT * FROM BookType where  ID =" + id + "");
            SqlCommand sqlCmd = new SqlCommand(_sqlCommand, _SQLConnection);
            SqlDataReader result;
            result = sqlCmd.ExecuteReader();
            while (result.Read())
            {
                ketqua = new DTO_BookType();
                ketqua.id = Convert.ToInt32(result["id"]);
                ketqua.name = result["name"].ToString();
                ketqua.note = result["note"].ToString();

            }
            _SQLConnection.Close();
            return ketqua;
        }
        


    }
}

