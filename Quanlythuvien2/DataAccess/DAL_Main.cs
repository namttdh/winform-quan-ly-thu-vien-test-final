﻿using Quanlythuvien.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quanlythuvien2.DataAccess
{
    class DAL_Main : Database
    {
        public DAL_Main()
        {
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
        }
        public Dictionary<String, int> getNumberOfStat()
        {            
            string _sqlCommand = string.Format("SELECT sum(amount) FROM [Book] SELECT count(id) FROM RoleMap Where idRole = 2 SELECT sum(amount) from[BookOrder]");
            SqlCommand sqlCmd = new SqlCommand(_sqlCommand, _SQLConnection);
            SqlDataReader resultQuerry = sqlCmd.ExecuteReader();
            Dictionary<String, int> result = new Dictionary<string, int>();
            while(resultQuerry.Read()) result.Add("totalBook" , DBNull.Value.Equals(resultQuerry[0])? 0: Convert.ToInt32(resultQuerry[0]));
            resultQuerry.NextResult();
            while (resultQuerry.Read()) result.Add("totalReader", DBNull.Value.Equals(resultQuerry[0]) ? 0:Convert.ToInt32(resultQuerry[0]));
            resultQuerry.NextResult();
            while (resultQuerry.Read()) result.Add("totalBookOrder", DBNull.Value.Equals(resultQuerry[0]) ? 0:Convert.ToInt32(resultQuerry[0]));
            
            
            _SQLConnection.Close();
            return result;
        }
    }
}
