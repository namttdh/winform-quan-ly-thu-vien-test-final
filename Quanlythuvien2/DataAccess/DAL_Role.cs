﻿using Quanlythuvien.Core;
using Quanlythuvien2.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quanlythuvien2.DataAccess
{
    class DAL_Role:Database
    {
        public DTO_Role GetRole(int id)
        {
            if(_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            String SQLCommand = String.Format("SELECT * FROM Role Where id={0}", id);
            SqlCommand sqlCmd = new SqlCommand(SQLCommand, _SQLConnection);
            SqlDataReader reader = sqlCmd.ExecuteReader();
            DTO_Role role = new DTO_Role();
            if (reader.Read() == true)
            {
                role.name = reader["name"].ToString();
                role.note = reader["note"].ToString();
                role.id = Convert.ToInt32(reader["id"]);
            }
            _SQLConnection.Close();
            return role;
        }
        public bool DeleteRole(int id)
        {
            bool Success = false;
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            try
            {
                String SQLCommand = String.Format("DELETE FROM Role Where id={0}", id);
                SqlCommand sqlCmd = new SqlCommand(SQLCommand, _SQLConnection);
                SqlDataReader reader = sqlCmd.ExecuteReader();
                Success = true;
            }
            catch
            {
                Success = false;
            }
            _SQLConnection.Close();
            return Success;
        }
        public bool AddRole(DTO_Role role)
        {
            bool Success = false;
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            string SQLCommand = string.Format("INSERT INTO Role(name,note) VALUES (N'{0}', N'{1}')",
                role.name,role.note);
            SqlCommand sqlcmd = new SqlCommand(SQLCommand, _SQLConnection);
            if (RoleNameCheck(role.name) == false)
            {
                if (_SQLConnection.State != ConnectionState.Open)
                {
                    _SQLConnection.Open();
                }
                sqlcmd.ExecuteNonQuery();
                return true;
            }
            _SQLConnection.Close();
            return Success;
        }
        public bool UpdateRole(DTO_Role role)
        {
            bool Success = false;
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }

            string SQLCommand = string.Format("UPDATE Role SET name = N'{0}', note = N'{1}' WHERE id = {2}",
                role.name,role.note,role.id);

            SqlCommand sqlcmd = new SqlCommand(SQLCommand, _SQLConnection);
            if (UpdateRoleNameCheck(role) == false)
            {
                if (_SQLConnection.State != ConnectionState.Open)
                {
                    _SQLConnection.Open();
                }
                sqlcmd.ExecuteNonQuery();
                return true;
            }
            _SQLConnection.Close();
            return Success;
        }
        public List<DTO_Role> GetAllRole()
        {
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            String SQLCommand = String.Format("SELECT * FROM Role");
            SqlCommand sqlCmd = new SqlCommand(SQLCommand, _SQLConnection);
            SqlDataReader reader = sqlCmd.ExecuteReader();
            List<DTO_Role> listRole = new List<DTO_Role>();
            while (reader.Read() == true)
            {
                DTO_Role role = new DTO_Role();
                role.name = reader["name"].ToString();
                role.note = reader["note"].ToString();
                role.id = Convert.ToInt32(reader["id"]);

                listRole.Add(role);
            }
            return listRole;
        }
        public List<DTO_Role> FindByName(string name)
        {
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            string SQLCommand = String.Format("SELECT * FROM Role Where name like N'%{0}%'", name);
            SqlCommand sqlCmd = new SqlCommand(SQLCommand, _SQLConnection);
            SqlDataReader reader = sqlCmd.ExecuteReader();
            List<DTO_Role> listRole = new List<DTO_Role>();
            while (reader.Read() == true)
            {
                DTO_Role role = new DTO_Role();
                role.name = reader["name"].ToString();
                role.note = reader["note"].ToString()==null ? " " : reader["note"].ToString();
                role.id = Convert.ToInt32(reader["id"]);
                listRole.Add(role);
            }
            return listRole;
        }
        public List<DTO_Role> FindByID(int id)
        {
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            string SQLCommand = String.Format("SELECT * FROM Role Where id ={0}", id);
            SqlCommand sqlCmd = new SqlCommand(SQLCommand, _SQLConnection);
            SqlDataReader reader = sqlCmd.ExecuteReader();
            List<DTO_Role> listRole = new List<DTO_Role>();
            while (reader.Read() == true)
            {
                DTO_Role role = new DTO_Role();
                role.name = reader["name"].ToString();
                role.note = reader["note"].ToString();
                role.id = Convert.ToInt32(reader["id"]);
                listRole.Add(role);
            }
            _SQLConnection.Close();
            return listRole;
        }
        public int AllRoleCount()
        {
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            int RoleCount = 0;
            string SQLCommand = String.Format("SELECT COUNT(*) FROM Role");
            SqlCommand sqlCmd = new SqlCommand(SQLCommand, _SQLConnection);
            RoleCount = (Int32)sqlCmd.ExecuteScalar();
            _SQLConnection.Close();
            return RoleCount;
        }
        public bool RoleNameCheck(String name)
        {
            bool isExist = false;
            String SQLCommand = String.Format("SELECT * FROM Role Where name = '{0}'", name);
            SqlCommand sqlCmd = new SqlCommand(SQLCommand, _SQLConnection);
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            SqlDataReader reader = sqlCmd.ExecuteReader();
            while (reader.Read() == true)
            {
                {
                    if (reader.HasRows)
                        isExist = true;
                    break;
                }
            }
            _SQLConnection.Close();
            return isExist;
        }
        public bool UpdateRoleNameCheck(DTO_Role role)
        {
            bool isExist = false;
            String SQLCommand = String.Format("SELECT * FROM Role Where name = '{0}' EXCEPT (SELECT * FROM Role Where Role.id = {1})", role.name,role.id);
            SqlCommand sqlCmd = new SqlCommand(SQLCommand, _SQLConnection);
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            SqlDataReader reader = sqlCmd.ExecuteReader();
            while (reader.Read() == true)
            {
                {
                    if (reader.HasRows)
                        isExist = true;
                    break;
                }
            }
            _SQLConnection.Close();
            return isExist;
        }
    }
}
