﻿using Quanlythuvien.Core;
using Quanlythuvien2.DTO;
using Quanlythuvien2.GUI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quanlythuvien2.DataAccess
{
    class DAL_RoleMap : Database
    {

        public DTO_RoleMap GetRoleMap(int id)
        {
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            String SQLCommand = String.Format("SELECT * FROM RoleMap Where idUser={0}", id);
            SqlCommand sqlCmd = new SqlCommand(SQLCommand, _SQLConnection);
            SqlDataReader reader = sqlCmd.ExecuteReader();
            DTO_RoleMap roleMap = new DTO_RoleMap();
            if (reader.Read() == true)
            {
                roleMap.idRole = Convert.ToInt32(reader["idRole"]);
                roleMap.idUser = Convert.ToInt32(reader["idUser"]);
                roleMap.id = Convert.ToInt32(reader["id"]);
            }
            return roleMap;
        }
        public List<int> GetRoleList(int id)
        {
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            String SQLCommand = String.Format("SELECT * FROM RoleMap Where idUser={0}", id);
            SqlCommand sqlCmd = new SqlCommand(SQLCommand, _SQLConnection);
            SqlDataReader reader = sqlCmd.ExecuteReader();
            List<int> listRole = new List<int>();
            while (reader.Read() == true)
            {
                listRole.Add((Int32)reader["idRole"]);
            }
            _SQLConnection.Close();
            return listRole;
        }
        public bool UpdateRoleMap(DTO_RoleMap roleMap)
        {
            bool Success = false;
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            string SQLCommand = string.Format("INSERT INTO RoleMap(idUser,idRole) VALUES ({0}, {1})",
                roleMap.idUser, roleMap.idRole);
            SqlCommand sqlcmd = new SqlCommand(SQLCommand, _SQLConnection);
            if (RoleNameCheck(roleMap.idUser, roleMap.idRole) == false)
            {
                if (_SQLConnection.State != ConnectionState.Open)
                {
                    _SQLConnection.Open();
                }
                sqlcmd.ExecuteNonQuery();
                return true;
            }
            _SQLConnection.Close();
            return Success;
        }
        public bool DeleteRoleMap(int idUser)
        {
            bool Success = false;
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            try
            {
                string SQLCommand = string.Format("DELETE FROM RoleMap WHERE idUser={0}",idUser);
                SqlCommand sqlcmd = new SqlCommand(SQLCommand, _SQLConnection);
                sqlcmd.ExecuteNonQuery();
                Success = true;
            }
            catch
            {
                Success= false;
            }
            _SQLConnection.Close();
            return Success;

        }
        public bool AddRoleMap(DTO_RoleMap roleMap)
        {
            bool Success = false;
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            string SQLCommand = string.Format("INSERT INTO RoleMap(idUser,idRole) VALUES ({0}, {1})",
                roleMap.idUser,roleMap.idRole);
            SqlCommand sqlcmd = new SqlCommand(SQLCommand, _SQLConnection);
            if (RoleNameCheck(roleMap.idUser,roleMap.idRole) == false)
            {
                if (_SQLConnection.State != ConnectionState.Open)
                {
                    _SQLConnection.Open();
                }
                sqlcmd.ExecuteNonQuery();
                return true;
            }
            _SQLConnection.Close();
            return Success;
        }


        public bool RoleNameCheck(int idUser, int idRole)
        {
            bool isExist = false;
            String SQLCommand = String.Format("SELECT * FROM RoleMap Where idUser={0} and idRole={1}", idUser,idRole);
            SqlCommand sqlCmd = new SqlCommand(SQLCommand, _SQLConnection);
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            SqlDataReader reader = sqlCmd.ExecuteReader();
            while (reader.Read() == true)
            {
                {
                    if (reader.HasRows)
                        isExist = true;
                    break;
                }
            }
            _SQLConnection.Close();
            return isExist;
        }
        public int Count(DTO_Role role)
        {
            int count = 0;
            String SQLCommand = String.Format("SELECT COUNT(*) FROM RoleMap where idRole={0}", role.id);
            SqlCommand sqlCmd = new SqlCommand(SQLCommand, _SQLConnection);

            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            count = (Int32)sqlCmd.ExecuteScalar();
            return count;
        }
    }
}
