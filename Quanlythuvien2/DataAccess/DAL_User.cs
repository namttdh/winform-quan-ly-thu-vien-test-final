﻿using Quanlythuvien.Core;
using Quanlythuvien2.Core;
using Quanlythuvien2.Object;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using UserAndUserGroup.DTO;

namespace Quanlythuvien.DataAccess
{
    class DAL_User : Database
    {

        public List<DTO_User> GetAllUser()
        {
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM [User]", _SQLConnection);
            DataTable dtUser = new DataTable();
            da.Fill(dtUser);
            List<DTO_User> listUser = ConvertToUser(dtUser);
            return listUser;
        }
        public DTO_User GetUser(int id)
        {
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            string SQLCommand = string.Format("SELECT * FROM [User] WHERE id={0}", id);
            SqlCommand sqlCmd = new SqlCommand(SQLCommand, _SQLConnection);
            SqlDataReader reader = sqlCmd.ExecuteReader();
            DTO_User user = new DTO_User();
            if (reader.Read() == true)
            {
                user.id = Convert.ToInt32(reader["id"]);
                user.name = reader["name"].ToString();
                user.username = reader["username"].ToString();
                user.password = reader["password"].ToString();
                user.address = reader["address"].ToString();
            }
            _SQLConnection.Close();
            return user;
        }
        public UserWithRole getUserByUsernameAndPass(String username, String password)
        {
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            string SQLCommand = string.Format("SELECT * FROM [User] WHERE username='{0}' and password = '{1}'", username, Helper.CreateMD5(password));
            SqlCommand sqlCmd = new SqlCommand(SQLCommand, _SQLConnection);
            SqlDataReader reader = sqlCmd.ExecuteReader();
            UserWithRole user = new UserWithRole();
            if (reader.Read() == true)
            {
                user.id = Convert.ToInt32(reader["id"]);
                user.name = reader["name"].ToString();
                user.username = reader["username"].ToString();
                user.password = reader["password"].ToString();
                user.address = reader["address"].ToString();
            }
            _SQLConnection.Close();
            return user;
        }
        public bool AddUser(DTO_User user,int idRole=2)
        {
            bool Success = false;
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            string SQLCommand = string.Format("INSERT INTO [User](username, password, name,address) VALUES (N'{0}', N'{1}', N'{2}', N'{3}')",
                user.username, Helper.CreateMD5(user.password), user.name, user.address);
            SqlCommand sqlcmd = new SqlCommand(SQLCommand, _SQLConnection);
            if (UsernameCheck(user.username) == false)
            {
                if (_SQLConnection.State != ConnectionState.Open)
                {
                    _SQLConnection.Open();
                }
                sqlcmd.ExecuteNonQuery();
                return true;
            }
            _SQLConnection.Close();
            return Success;
        }
        public bool Update(DTO_User user)
        {
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            string SQLCommand = "";
            if (user.password != null)
            {
                SQLCommand = string.Format("UPDATE [User] SET username = N'{0}', password = N'{1}'," +
                " name = N'{2}', address = N'{3}' WHERE id = {4}",
                user.username, Helper.CreateMD5(user.password), user.name, user.address, user.id);
            }
            else
            {
                SQLCommand = string.Format("UPDATE [User] SET username = N'{0}'," +
                " name = N'{1}', address = N'{2}' WHERE id = {3}",
                user.username, user.name, user.address, user.id);
            }           

            
            SqlCommand sqlcmd = new SqlCommand(SQLCommand, _SQLConnection);
            //if (UsernameCheck(user.username)==false)
            //{
            //    if (_SQLConnection.State != ConnectionState.Open)
            //    {
            //        _SQLConnection.Open();
            //    }
            //    sqlcmd.ExecuteNonQuery();
            //    return true;
            //}
            sqlcmd.ExecuteNonQuery();
            _SQLConnection.Close();
            return true;
        }
        public void DeleteUser(int id)
        {
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            string SQLCommand2= string.Format("DELETE FROM RoleMap WHERE idUser={0}", id);
            string SQLCommand = string.Format("DELETE FROM [User] WHERE id={0}", id);
            SqlCommand sqlcmd = new SqlCommand(SQLCommand, _SQLConnection);
            SqlCommand sqlcmd2 = new SqlCommand(SQLCommand2, _SQLConnection);
            sqlcmd2.ExecuteNonQuery();
            sqlcmd.ExecuteNonQuery();

            _SQLConnection.Close();
        }
        public List<DTO_User> ConvertToUser(DataTable datatable)
        {
            List<DTO_User> listUser = new List<DTO_User>();
            foreach (DataRow rowData in datatable.Rows)
            {
                DTO_User user = new DTO_User();
                user.id = Convert.ToInt32(rowData["id"]);
                user.name = rowData["name"].ToString();
                user.username = rowData["username"].ToString();
                user.password = rowData["password"].ToString();
                user.address = rowData["address"].ToString();
                listUser.Add(user);
            }
            return listUser;
        }
        public List<DTO_User> FindByName(string name)
        {
            if(_SQLConnection.State!= ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            string SQLCommand = String.Format("SELECT * FROM [User] Where name like N'%{0}%'", name);
            SqlDataAdapter da = new SqlDataAdapter(SQLCommand, _SQLConnection);
            DataTable dtUser = new DataTable();
            da.Fill(dtUser);
            List<DTO_User> listUser = ConvertToUser(dtUser);
            return listUser;
        }
        public DTO_User FindByUsername(string name)
        {
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            string SQLCommand = String.Format("SELECT * FROM [User] Where username = '{0}'", name);
            SqlCommand sqlCmd = new SqlCommand(SQLCommand, _SQLConnection);
            SqlDataReader reader = sqlCmd.ExecuteReader();
            DTO_User user = new DTO_User();
            if (reader.Read() == true)
            {
                user.id = Convert.ToInt32(reader["id"]);
                user.name = reader["name"].ToString();
                user.username = reader["username"].ToString();
                user.password = reader["password"].ToString();
                user.address = reader["address"].ToString();
            }
            _SQLConnection.Close();
            return user;
        }
        public List<DTO_User> FindByID(int id)
        {
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            string SQLCommand = String.Format("SELECT * FROM [User] Where id ={0}", id);
            SqlDataAdapter da = new SqlDataAdapter(SQLCommand, _SQLConnection);
            DataTable dtUser = new DataTable();
            da.Fill(dtUser);
            List<DTO_User> listUser = ConvertToUser(dtUser);
            return listUser;
        }
        public List<DTO_User> FindByAddress(string address)
        {
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            string SQLCommand = String.Format("SELECT * FROM [User] Where address like N'%{0}%'", address);
            SqlDataAdapter da = new SqlDataAdapter(SQLCommand, _SQLConnection);
            DataTable dtUser = new DataTable();
            da.Fill(dtUser);
            List<DTO_User> listUser = ConvertToUser(dtUser);
            _SQLConnection.Close();
            return listUser;
        }
        public int AllUserCount()
        {
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            int UserCount=0;
            string SQLCommand = String.Format("SELECT COUNT(*) FROM [User]");
            SqlCommand sqlCmd = new SqlCommand(SQLCommand, _SQLConnection);
            UserCount=(Int32)sqlCmd.ExecuteScalar();
            return UserCount;
        }


        public bool UsernameCheck(String name)
        {
            bool isExist = false;
            String SQLCommand = String.Format("SELECT * FROM [User] Where username = '{0}'", name);
            SqlCommand sqlCmd = new SqlCommand(SQLCommand, _SQLConnection);
            if (_SQLConnection.State != ConnectionState.Open)
            {
                _SQLConnection.Open();
            }
            SqlDataReader reader = sqlCmd.ExecuteReader();
            while (reader.Read() == true)
            {
                {
                    if(reader.HasRows)
                    isExist = true;
                    break;
                }
            }
            _SQLConnection.Close();
            return isExist;
        }
    }
}
