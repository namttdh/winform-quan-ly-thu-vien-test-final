﻿namespace Quanlythuvien2
{
    partial class FormAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.hệThốngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thànhViênToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sáchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borrowBookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.typeBookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formUserMenuStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.lbLeftBook = new System.Windows.Forms.Label();
            this.lbReader = new System.Windows.Forms.Label();
            this.lbBookBorrow = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.orderData = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.userData = new System.Windows.Forms.DataGridView();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderData)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userData)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hệThốngToolStripMenuItem,
            this.thànhViênToolStripMenuItem,
            this.sáchToolStripMenuItem,
            this.formUserMenuStrip});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1040, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // hệThốngToolStripMenuItem
            // 
            this.hệThốngToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logoutToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.hệThốngToolStripMenuItem.Name = "hệThốngToolStripMenuItem";
            this.hệThốngToolStripMenuItem.Size = new System.Drawing.Size(83, 24);
            this.hệThốngToolStripMenuItem.Text = "Hệ thống";
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(152, 26);
            this.logoutToolStripMenuItem.Text = "Đăng xuất";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 26);
            this.exitToolStripMenuItem.Text = "Thoát";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // thànhViênToolStripMenuItem
            // 
            this.thànhViênToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userToolStripMenuItem,
            this.groupUserToolStripMenuItem});
            this.thànhViênToolStripMenuItem.Name = "thànhViênToolStripMenuItem";
            this.thànhViênToolStripMenuItem.Size = new System.Drawing.Size(92, 24);
            this.thànhViênToolStripMenuItem.Text = "Thành viên";
            // 
            // userToolStripMenuItem
            // 
            this.userToolStripMenuItem.Name = "userToolStripMenuItem";
            this.userToolStripMenuItem.Size = new System.Drawing.Size(248, 26);
            this.userToolStripMenuItem.Text = "Quản lý thành viên";
            this.userToolStripMenuItem.Click += new System.EventHandler(this.userToolStripMenuItem_Click);
            // 
            // groupUserToolStripMenuItem
            // 
            this.groupUserToolStripMenuItem.Name = "groupUserToolStripMenuItem";
            this.groupUserToolStripMenuItem.Size = new System.Drawing.Size(248, 26);
            this.groupUserToolStripMenuItem.Text = "Quản lý nhóm thành viên";
            this.groupUserToolStripMenuItem.Click += new System.EventHandler(this.groupUserToolStripMenuItem_Click);
            // 
            // sáchToolStripMenuItem
            // 
            this.sáchToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bookToolStripMenuItem,
            this.borrowBookToolStripMenuItem,
            this.typeBookToolStripMenuItem,
            this.groupBookToolStripMenuItem});
            this.sáchToolStripMenuItem.Name = "sáchToolStripMenuItem";
            this.sáchToolStripMenuItem.Size = new System.Drawing.Size(52, 24);
            this.sáchToolStripMenuItem.Text = "Sách";
            // 
            // bookToolStripMenuItem
            // 
            this.bookToolStripMenuItem.Name = "bookToolStripMenuItem";
            this.bookToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.bookToolStripMenuItem.Text = "Quản lý kho sách";
            this.bookToolStripMenuItem.Click += new System.EventHandler(this.bookToolStripMenuItem_Click);
            // 
            // borrowBookToolStripMenuItem
            // 
            this.borrowBookToolStripMenuItem.Name = "borrowBookToolStripMenuItem";
            this.borrowBookToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.borrowBookToolStripMenuItem.Text = "Quản lý mượn trả";
            this.borrowBookToolStripMenuItem.Click += new System.EventHandler(this.borrowBookToolStripMenuItem_Click);
            // 
            // typeBookToolStripMenuItem
            // 
            this.typeBookToolStripMenuItem.Name = "typeBookToolStripMenuItem";
            this.typeBookToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.typeBookToolStripMenuItem.Text = "Quản lý thể loại sách";
            this.typeBookToolStripMenuItem.Click += new System.EventHandler(this.typeBookToolStripMenuItem_Click);
            // 
            // groupBookToolStripMenuItem
            // 
            this.groupBookToolStripMenuItem.Name = "groupBookToolStripMenuItem";
            this.groupBookToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.groupBookToolStripMenuItem.Text = "Quản lý nhóm sách";
            this.groupBookToolStripMenuItem.Click += new System.EventHandler(this.groupBookToolStripMenuItem_Click);
            // 
            // formUserMenuStrip
            // 
            this.formUserMenuStrip.Name = "formUserMenuStrip";
            this.formUserMenuStrip.Size = new System.Drawing.Size(157, 24);
            this.formUserMenuStrip.Text = "Giao diện thành viên";
            this.formUserMenuStrip.Click += new System.EventHandler(this.formUserMenuStrip_Click);
            // 
            // lbLeftBook
            // 
            this.lbLeftBook.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbLeftBook.Location = new System.Drawing.Point(12, 55);
            this.lbLeftBook.Name = "lbLeftBook";
            this.lbLeftBook.Size = new System.Drawing.Size(339, 69);
            this.lbLeftBook.TabIndex = 1;
            this.lbLeftBook.Text = "Tổng số sách còn: \r\n1000";
            this.lbLeftBook.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbReader
            // 
            this.lbReader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbReader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbReader.Location = new System.Drawing.Point(363, 55);
            this.lbReader.Name = "lbReader";
            this.lbReader.Size = new System.Drawing.Size(310, 69);
            this.lbReader.TabIndex = 1;
            this.lbReader.Text = "Tổng số độc giả: \r\n1000";
            this.lbReader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbBookBorrow
            // 
            this.lbBookBorrow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbBookBorrow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbBookBorrow.Location = new System.Drawing.Point(686, 55);
            this.lbBookBorrow.Name = "lbBookBorrow";
            this.lbBookBorrow.Size = new System.Drawing.Size(339, 69);
            this.lbBookBorrow.TabIndex = 1;
            this.lbBookBorrow.Text = "Số sách đang cho mượn: \r\n1000";
            this.lbBookBorrow.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.orderData);
            this.groupBox1.Location = new System.Drawing.Point(13, 155);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(590, 389);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Đơn mượn sách mới nhất";
            // 
            // orderData
            // 
            this.orderData.AllowUserToAddRows = false;
            this.orderData.AllowUserToDeleteRows = false;
            this.orderData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.orderData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orderData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.orderData.Location = new System.Drawing.Point(3, 18);
            this.orderData.MultiSelect = false;
            this.orderData.Name = "orderData";
            this.orderData.ReadOnly = true;
            this.orderData.RowHeadersVisible = false;
            this.orderData.RowTemplate.Height = 24;
            this.orderData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.orderData.ShowCellErrors = false;
            this.orderData.ShowCellToolTips = false;
            this.orderData.ShowEditingIcon = false;
            this.orderData.ShowRowErrors = false;
            this.orderData.Size = new System.Drawing.Size(584, 368);
            this.orderData.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.userData);
            this.groupBox2.Location = new System.Drawing.Point(609, 155);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(419, 389);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Người dùng mới nhất";
            // 
            // userData
            // 
            this.userData.AllowUserToAddRows = false;
            this.userData.AllowUserToDeleteRows = false;
            this.userData.AllowUserToOrderColumns = true;
            this.userData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.userData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.userData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userData.Location = new System.Drawing.Point(3, 18);
            this.userData.Name = "userData";
            this.userData.ReadOnly = true;
            this.userData.RowHeadersVisible = false;
            this.userData.RowTemplate.Height = 24;
            this.userData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.userData.Size = new System.Drawing.Size(413, 368);
            this.userData.TabIndex = 0;
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 547);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1040, 25);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(411, 20);
            this.toolStripStatusLabel1.Text = "Người dùng \"ABC\" - Quyền \"ZYX\" - Đăng nhập lúc \"12:12:12\"";
            // 
            // FormAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1040, 572);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbBookBorrow);
            this.Controls.Add(this.lbReader);
            this.Controls.Add(this.lbLeftBook);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormAdmin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý thư viện";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormAdmin_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.orderData)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.userData)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem hệThốngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thànhViênToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem groupUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sáchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bookToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem borrowBookToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem typeBookToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem groupBookToolStripMenuItem;
        private System.Windows.Forms.Label lbLeftBook;
        private System.Windows.Forms.Label lbReader;
        private System.Windows.Forms.Label lbBookBorrow;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView userData;
        private System.Windows.Forms.DataGridView orderData;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripMenuItem formUserMenuStrip;
    }
}

