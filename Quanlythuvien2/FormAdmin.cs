﻿using LibraryManager.GUI;
using Quanlythuvien.DataAccess;
using Quanlythuvien2.Core;
using Quanlythuvien2.DataAccess;
using Quanlythuvien2.DTO;
using Quanlythuvien2.GUI;
using Quanlythuvien2.Object;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UserAndUserGroup;
using UserAndUserGroup.DTO;

namespace Quanlythuvien2
{
    public partial class FormAdmin : Form
    {
        public LoginForm loginForm;
        DAL_Main dalMain = new DAL_Main();
        DAL_BookOrder dalBookOrder = new DAL_BookOrder();
        DAL_User dalUser = new DAL_User();
        public FormAdmin()
        {
            InitializeComponent();
            init();
        }
        void init()
        {
            createGridUser();
            createGridOrder();
            getStat();
        }

        private void getStat()
        {
            Dictionary<String,int> result = dalMain.getNumberOfStat();
            lbLeftBook.Text = String.Format("Số sách còn trong thư viện: \r\n {0}", result["totalBook"]);
            lbReader.Text = String.Format("Số độc giả: \r\n {0}", result["totalReader"]);
            lbBookBorrow.Text = String.Format("Số sách đang cho mượn: \r\n {0}", result["totalBookOrder"]);
        }

        private void createGridOrder()
        {
            orderData.Columns.Add("id", "ID");
            orderData.Columns["id"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            orderData.Columns.Add("name", "Tên người thuê");
            orderData.Columns.Add("date", "Ngày thuê");
            orderData.Columns.Add("datePromis", "Ngày hẹn trả");
            orderData.Columns.Add("amount", "Số lượng");
            orderData.Columns["amount"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            List<BookOrderWithName> listOrder = dalBookOrder.getAllBookOrder(true);
            foreach(BookOrderWithName bookOrder in listOrder){
                orderData.Rows.Add(bookOrder.id, bookOrder.name, Helper.getTimeFromUnixTime(bookOrder.date), Helper.getTimeFromUnixTime(bookOrder.datePromise),bookOrder.amount);
            }
            //Console.WriteLine(Helper.getDateTimeFromUnixTimeStamp(listOrder[0].datePromise).ToString("dd/MM/yyyy"));
        }

        private void createGridUser()
        {
            userData.Columns.Add("id", "ID");
            userData.Columns.Add("username", "Username");
            userData.Columns.Add("name", "Tên");
            userData.Columns.Add("address", "Địa chỉ");
            List<DTO_User> listUser = dalUser.GetAllUser();
            foreach (DTO_User user in listUser)
            {
                userData.Rows.Add(user.id, user.username, user.name, user.address);
            }
            userData.Sort(this.userData.Columns["id"], ListSortDirection.Descending);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void userToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formUser frmUser = new formUser();
            frmUser.Show();
            //this.Close();
        }

        private void groupUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GroupUser groupUser = new GroupUser();
            groupUser.Show();
        }

        private void bookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUI_Book_ guiBook = new GUI_Book_();
            guiBook.Show();
        }

        private void borrowBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormBorrow borrow = new FormBorrow();
            borrow.Show();
        }

        private void typeBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUI_TypeBook_ bookType = new GUI_TypeBook_();
            bookType.Show();
        }

        private void groupBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUI_GroupBook_ bookGroup = new GUI_GroupBook_();
            bookGroup.Show();
        }

        private void formUserMenuStrip_Click(object sender, EventArgs e)
        {
            FormUser formUser = new FormUser();
            formUser.Show();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loginForm.Show();
            this.Hide();

        }

        private void FormAdmin_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
            
        }
    }
}
