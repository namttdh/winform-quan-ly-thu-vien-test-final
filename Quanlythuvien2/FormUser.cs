﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quanlythuvien2
{
    public partial class FormUser : Form
    {
        public LoginForm loginForm;
        public FormUser()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loginForm.Show();
            this.Hide();
        }

        private void FormUser_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void changeInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
