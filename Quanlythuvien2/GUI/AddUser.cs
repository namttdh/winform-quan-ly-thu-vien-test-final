﻿using Quanlythuvien.DataAccess;
using Quanlythuvien2.DataAccess;
using Quanlythuvien2.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UserAndUserGroup.DTO;

namespace Quanlythuvien2.GUI
{
    public partial class AddUser : Form
    {
        List<DTO_Role> listRole;
        public AddUser()
        {
            InitializeComponent();
            listBoxData();
        }
        public void listBoxData()
        {
            DAL_Role dalRole = new DAL_Role();
            listRole = dalRole.GetAllRole();
            foreach(DTO_Role role in listRole)
            {
                listBox.Items.Add(role.name);
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            DTO_User user = new DTO_User()
            {
                name = tbName.Text,
                username = tbUsername.Text,
                password = tbPassword.Text,
                address = tbAddress.Text
            };
            DAL_User dalUser = new DAL_User();
            DAL_Role dalRole = new DAL_Role();
            DAL_RoleMap dalRoleMap = new DAL_RoleMap();
            if (dalUser.AddUser(user))
            {
                DTO_User newUser = dalUser.FindByUsername(user.username);
                for (int i = 0; i < listBox.Items.Count; i++)
                {
                    if (listBox.GetSelected(i) == true)
                    {
                        DTO_RoleMap roleMap = new DTO_RoleMap()
                        {
                            idUser = newUser.id,
                            idRole = listRole[i].id
                        };
                    dalRoleMap.AddRoleMap(roleMap);
                    }
                }
                MessageBox.Show("Thêm thành công");
                tbName.Clear();
                tbUsername.Clear();
                tbPassword.Clear();
                tbAddress.Clear();
            }
            else
            {
                MessageBox.Show("Thêm thất bại, hãy chọn tên đăng nhập khác");
            }
        }

        private void AddUser_Load(object sender, EventArgs e)
        {

        }

        private void tbName_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
