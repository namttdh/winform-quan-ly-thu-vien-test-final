﻿using Quanlythuvien2.DataAccess;
using Quanlythuvien2.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quanlythuvien2.GUI
{
    public partial class AddUserGroup : Form
    {
        public AddUserGroup()
        {
            InitializeComponent();
        }

        private void buttonAddGr_Click(object sender, EventArgs e)
        {
            DTO_Role role = new DTO_Role()
            {
                name = textName.Text,
                note = textNote.Text
            };
            DAL_Role dalRole = new DAL_Role();
            if (dalRole.AddRole(role))
            {
                MessageBox.Show("Thêm thành công");
            }
            else
            {
                MessageBox.Show("Thêm thất bại");
            }
        }
    }
}
