﻿namespace Quanlythuvien2
{
    partial class FormBorrow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dataListOrder = new System.Windows.Forms.DataGridView();
            this.dateTimeEnd = new System.Windows.Forms.DateTimePicker();
            this.cbExpried = new System.Windows.Forms.CheckBox();
            this.dateTimeStart = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnNotReceive = new System.Windows.Forms.Button();
            this.btnReceive = new System.Windows.Forms.Button();
            this.openBrrowForm = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnUnFind = new System.Windows.Forms.Button();
            this.btnFind = new System.Windows.Forms.Button();
            this.txtFind = new System.Windows.Forms.TextBox();
            this.cbFindBy = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupStat = new System.Windows.Forms.GroupBox();
            this.lbReturn = new System.Windows.Forms.Label();
            this.lbBorrow = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataListOrder)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupStat.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.dataListOrder);
            this.groupBox1.Controls.Add(this.dateTimeEnd);
            this.groupBox1.Controls.Add(this.cbExpried);
            this.groupBox1.Controls.Add(this.dateTimeStart);
            this.groupBox1.Location = new System.Drawing.Point(12, 114);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(812, 446);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Danh sách đơn mượn";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(587, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "-";
            // 
            // dataListOrder
            // 
            this.dataListOrder.AllowUserToAddRows = false;
            this.dataListOrder.AllowUserToDeleteRows = false;
            this.dataListOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataListOrder.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataListOrder.BackgroundColor = System.Drawing.Color.White;
            this.dataListOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataListOrder.Location = new System.Drawing.Point(5, 48);
            this.dataListOrder.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataListOrder.Name = "dataListOrder";
            this.dataListOrder.ReadOnly = true;
            this.dataListOrder.RowHeadersVisible = false;
            this.dataListOrder.RowTemplate.Height = 24;
            this.dataListOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataListOrder.Size = new System.Drawing.Size(800, 391);
            this.dataListOrder.TabIndex = 1;
            // 
            // dateTimeEnd
            // 
            this.dateTimeEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimeEnd.Location = new System.Drawing.Point(604, 17);
            this.dateTimeEnd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateTimeEnd.Name = "dateTimeEnd";
            this.dateTimeEnd.Size = new System.Drawing.Size(200, 22);
            this.dateTimeEnd.TabIndex = 0;
            this.dateTimeEnd.ValueChanged += new System.EventHandler(this.dateTimeEnd_ValueChanged);
            this.dateTimeEnd.Enter += new System.EventHandler(this.dateTimeEnd_Enter);
            // 
            // cbExpried
            // 
            this.cbExpried.AutoSize = true;
            this.cbExpried.Location = new System.Drawing.Point(5, 21);
            this.cbExpried.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbExpried.Name = "cbExpried";
            this.cbExpried.Size = new System.Drawing.Size(216, 21);
            this.cbExpried.TabIndex = 0;
            this.cbExpried.Text = "Hiển thị đơn sách đã quá hạn";
            this.cbExpried.UseVisualStyleBackColor = true;
            this.cbExpried.CheckedChanged += new System.EventHandler(this.cbExpried_CheckedChanged);
            // 
            // dateTimeStart
            // 
            this.dateTimeStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimeStart.Location = new System.Drawing.Point(381, 17);
            this.dateTimeStart.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateTimeStart.Name = "dateTimeStart";
            this.dateTimeStart.Size = new System.Drawing.Size(200, 22);
            this.dateTimeStart.TabIndex = 0;
            this.dateTimeStart.ValueChanged += new System.EventHandler(this.dateTimeStart_ValueChanged);
            this.dateTimeStart.Enter += new System.EventHandler(this.dateTimeStart_Enter);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnDelete);
            this.groupBox2.Controls.Add(this.btnEdit);
            this.groupBox2.Controls.Add(this.btnNotReceive);
            this.groupBox2.Controls.Add(this.btnReceive);
            this.groupBox2.Controls.Add(this.openBrrowForm);
            this.groupBox2.Location = new System.Drawing.Point(829, 114);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(200, 446);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Xử lý";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(7, 329);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(187, 65);
            this.btnDelete.TabIndex = 0;
            this.btnDelete.Text = "Xoá";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(7, 258);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(187, 65);
            this.btnEdit.TabIndex = 0;
            this.btnEdit.Text = "Sửa / Xem chi tiết";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnNotReceive
            // 
            this.btnNotReceive.Location = new System.Drawing.Point(7, 188);
            this.btnNotReceive.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnNotReceive.Name = "btnNotReceive";
            this.btnNotReceive.Size = new System.Drawing.Size(187, 65);
            this.btnNotReceive.TabIndex = 0;
            this.btnNotReceive.Text = "Đánh dấu chưa trả";
            this.btnNotReceive.UseVisualStyleBackColor = true;
            this.btnNotReceive.Click += new System.EventHandler(this.btnNotReceive_Click);
            // 
            // btnReceive
            // 
            this.btnReceive.Location = new System.Drawing.Point(7, 119);
            this.btnReceive.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnReceive.Name = "btnReceive";
            this.btnReceive.Size = new System.Drawing.Size(187, 65);
            this.btnReceive.TabIndex = 0;
            this.btnReceive.Text = "Đánh dấu đã trả";
            this.btnReceive.UseVisualStyleBackColor = true;
            this.btnReceive.Click += new System.EventHandler(this.btnReceive_Click);
            // 
            // openBrrowForm
            // 
            this.openBrrowForm.Location = new System.Drawing.Point(7, 48);
            this.openBrrowForm.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.openBrrowForm.Name = "openBrrowForm";
            this.openBrrowForm.Size = new System.Drawing.Size(187, 65);
            this.openBrrowForm.TabIndex = 0;
            this.openBrrowForm.Text = "Cho mượn";
            this.openBrrowForm.UseVisualStyleBackColor = true;
            this.openBrrowForm.Click += new System.EventHandler(this.openBrrowForm_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.btnUnFind);
            this.groupBox3.Controls.Add(this.btnFind);
            this.groupBox3.Controls.Add(this.txtFind);
            this.groupBox3.Controls.Add(this.cbFindBy);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(12, 6);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(613, 103);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tra cứu";
            // 
            // btnUnFind
            // 
            this.btnUnFind.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUnFind.Location = new System.Drawing.Point(351, 64);
            this.btnUnFind.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUnFind.Name = "btnUnFind";
            this.btnUnFind.Size = new System.Drawing.Size(120, 31);
            this.btnUnFind.TabIndex = 4;
            this.btnUnFind.Text = "Huỷ tìm kiếm";
            this.btnUnFind.UseVisualStyleBackColor = true;
            this.btnUnFind.Click += new System.EventHandler(this.btnUnFind_Click);
            // 
            // btnFind
            // 
            this.btnFind.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFind.Location = new System.Drawing.Point(477, 64);
            this.btnFind.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(120, 31);
            this.btnFind.TabIndex = 4;
            this.btnFind.Text = "Tìm kiếm";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // txtFind
            // 
            this.txtFind.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFind.Location = new System.Drawing.Point(150, 36);
            this.txtFind.Margin = new System.Windows.Forms.Padding(4);
            this.txtFind.Name = "txtFind";
            this.txtFind.Size = new System.Drawing.Size(447, 22);
            this.txtFind.TabIndex = 3;
            // 
            // cbFindBy
            // 
            this.cbFindBy.DisplayMember = "fsdf";
            this.cbFindBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFindBy.FormattingEnabled = true;
            this.cbFindBy.Items.AddRange(new object[] {
            "Tên người mượn",
            "ID"});
            this.cbFindBy.Location = new System.Drawing.Point(12, 34);
            this.cbFindBy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbFindBy.Name = "cbFindBy";
            this.cbFindBy.Size = new System.Drawing.Size(133, 24);
            this.cbFindBy.TabIndex = 2;
            this.cbFindBy.SelectedIndexChanged += new System.EventHandler(this.cbFindBy_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 17);
            this.label1.TabIndex = 1;
            // 
            // groupStat
            // 
            this.groupStat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupStat.Controls.Add(this.lbReturn);
            this.groupStat.Controls.Add(this.lbBorrow);
            this.groupStat.Location = new System.Drawing.Point(631, 6);
            this.groupStat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupStat.Name = "groupStat";
            this.groupStat.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupStat.Size = new System.Drawing.Size(397, 103);
            this.groupStat.TabIndex = 3;
            this.groupStat.TabStop = false;
            this.groupStat.Text = "Thông kê";
            // 
            // lbReturn
            // 
            this.lbReturn.AutoSize = true;
            this.lbReturn.Location = new System.Drawing.Point(7, 44);
            this.lbReturn.Name = "lbReturn";
            this.lbReturn.Size = new System.Drawing.Size(113, 17);
            this.lbReturn.TabIndex = 0;
            this.lbReturn.Text = "Đã trả: 100 sách";
            // 
            // lbBorrow
            // 
            this.lbBorrow.AutoSize = true;
            this.lbBorrow.Location = new System.Drawing.Point(7, 22);
            this.lbBorrow.Name = "lbBorrow";
            this.lbBorrow.Size = new System.Drawing.Size(139, 17);
            this.lbBorrow.TabIndex = 0;
            this.lbBorrow.Text = "Đã mượn: 1000 sách";
            // 
            // FormBorrow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1040, 572);
            this.Controls.Add(this.groupStat);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FormBorrow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý đơn sách";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataListOrder)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupStat.ResumeLayout(false);
            this.groupStat.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dataListOrder;
        private System.Windows.Forms.CheckBox cbExpried;
        private System.Windows.Forms.DateTimePicker dateTimeEnd;
        private System.Windows.Forms.DateTimePicker dateTimeStart;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnReceive;
        private System.Windows.Forms.Button openBrrowForm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupStat;
        private System.Windows.Forms.Label lbReturn;
        private System.Windows.Forms.Label lbBorrow;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.TextBox txtFind;
        private System.Windows.Forms.ComboBox cbFindBy;
        private System.Windows.Forms.Button btnUnFind;
        private System.Windows.Forms.Button btnNotReceive;
    }
}