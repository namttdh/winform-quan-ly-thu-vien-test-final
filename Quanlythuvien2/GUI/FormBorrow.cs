﻿using Quanlythuvien2.Core;
using Quanlythuvien2.DataAccess;
using Quanlythuvien2.Object;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quanlythuvien2
{
    public partial class FormBorrow : Form
    {
        DAL_BookOrder dalBookOrder = new DAL_BookOrder();
        Dictionary<String, String> filter = new Dictionary<string, string>();
        DateTime lastTimeStart;
        DateTime lastTimeEnd;
        public FormBorrow()
        {
            InitializeComponent();            
            createDataGridListOrder();
            cbFindBy.SelectedIndex = 0;
            dateTimeStart.Value = DateTime.Now.AddDays(-30);
            dateTimeStart.MaxDate = DateTime.Now;
            dateTimeEnd.MaxDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
        }
        void changeFollowData()
        {
            if(dateTimeStart.Value.ToString("dd/MM/yyyy").Equals(dateTimeEnd.Value.ToString("dd/MM/yyyy")))
            {
                if(groupStat.Enabled) groupStat.Text = "Thông kê trong ngày " + dateTimeStart.Value.ToString("dd/MM/yyyy");
                dateTimeStart.Value = new DateTime(dateTimeStart.Value.Year, dateTimeStart.Value.Month, dateTimeStart.Value.Day, 0, 0, 0, 0);
                if (dateTimeEnd.Value.ToString("dd/MM/yyyy").Contains(DateTime.Now.ToString("dd/MM/yyyy")))
                {
                    DateTime now = DateTime.Now;
                    dateTimeEnd.Value = new DateTime(dateTimeStart.Value.Year, dateTimeStart.Value.Month, dateTimeStart.Value.Day, now.Hour, now.Minute, now.Second, now.Millisecond);
                }
                else
                {
                    dateTimeEnd.Value = new DateTime(dateTimeStart.Value.Year, dateTimeStart.Value.Month, dateTimeStart.Value.Day, 23, 59, 59, 999);
                }
                        
            }
            else
            {
                if (groupStat.Enabled) groupStat.Text = "Thông kê từ " + dateTimeStart.Value.ToString("dd/MM/yyyy") + " - " + dateTimeEnd.Value.ToString("dd/MM/yyyy");               
            }
            loadData(dalBookOrder.getBookOrderByTimeAndMaybeName(Helper.ConvertToUnixTime(dateTimeStart.Value), Helper.ConvertToUnixTime(dateTimeEnd.Value), filter.ContainsKey("name") ? filter["name"] : "", cbExpried.Checked, true));
            if (filter.Count > 0) groupStat.Text += " - Theo tra cứu";

        }
        private void dateTimeStart_ValueChanged(object sender, EventArgs e)
        {
            dateTimeStart.MaxDate = dateTimeEnd.Value;
            dateTimeEnd.MinDate = dateTimeStart.Value;
            changeFollowData();


        }

        private void dateTimeEnd_ValueChanged(object sender, EventArgs e)
        {
            dateTimeStart.MaxDate = dateTimeEnd.Value;
            dateTimeEnd.MinDate = dateTimeStart.Value;
            changeFollowData();
        }

        private void dateTimeStart_Enter(object sender, EventArgs e)
        {
            lastTimeStart = dateTimeStart.Value;
        }

        private void dateTimeEnd_Enter(object sender, EventArgs e)
        {
            lastTimeEnd = dateTimeEnd.Value;
        }
        private void createDataGridListOrder()
        {
            dataListOrder.Columns.Add("id", "ID");
            dataListOrder.Columns["id"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataListOrder.Columns.Add("name", "Tên người mượn");
            dataListOrder.Columns.Add("date", "Ngày mượn");
            dataListOrder.Columns.Add("datePromise", "Ngày hứa trả");
            dataListOrder.Columns.Add("dateReturn", "Ngày trả");
            dataListOrder.Columns.Add("amount", "Số sách mượn");
            dataListOrder.Columns["amount"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
        }
        private void loadData(List<BookOrderWithName> data)
        {
            dataListOrder.Rows.Clear();
            foreach(BookOrderWithName bookOrder in data)
            {
                dataListOrder.Rows.Add(bookOrder.id, bookOrder.name, Helper.getTimeFromUnixTime(bookOrder.date), Helper.getTimeFromUnixTime(bookOrder.datePromise), (bookOrder.dateReturn != 0)? Helper.getTimeFromUnixTime(bookOrder.dateReturn) : "", bookOrder.amount);
            }
            dataListOrder.Refresh();
            if(groupStat.Enabled) lbBorrow.Text = String.Format("Đã mượn: {0} sách", (from x in data select x.amount).Sum());
            if (groupStat.Enabled) lbReturn.Text = String.Format("Đã trả: {0} sách", (from x in data where x.dateReturn != 0 select x.amount).Sum());
        }

        private void cbExpried_CheckedChanged(object sender, EventArgs e)
        {
            if (cbExpried.Checked)
            {
                groupStat.Enabled = false;
            }
            else
            {
                groupStat.Enabled = true;
            }
            changeFollowData();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if(dataListOrder.SelectedRows.Count < 1)
            {
                MessageBox.Show("Vui lòng chọn đơn sách mà bạn cần xoá");
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Bạn có chắc chắn muốn xoá "+ dataListOrder.SelectedRows.Count + " đơn sách?", "Cảnh báo", MessageBoxButtons.YesNo);
                if(dialogResult == DialogResult.Yes)
                {
                    String[] id = new string[dataListOrder.SelectedRows.Count];
                    int i = 0;
                    foreach (DataGridViewRow rows in dataListOrder.SelectedRows)
                    {

                        id[i] = rows.Cells["id"].Value.ToString();
                        dataListOrder.Rows.RemoveAt(rows.Index);
                        i++;
                    }
                    dalBookOrder.deleteBookOrderMap(id);
                    dalBookOrder.deleteByListId(id);
                }
                
            }
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            filter.Clear();
            if(cbFindBy.SelectedIndex == 0)
            {
                filter.Add("name", txtFind.Text);
                changeFollowData();
            }
            else if(cbFindBy.SelectedIndex == 1)
            {
                if(!int.TryParse(txtFind.Text, out int n))
                {
                    MessageBox.Show("Sai định dang của ID");
                }
                else
                {
                    loadData(dalBookOrder.findById(txtFind.Text));
                }
                
            }
        }

        private void cbFindBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbFindBy.SelectedIndex == 1)
            {
                dateTimeEnd.Enabled = false;
                dateTimeStart.Enabled = false;
                cbExpried.Enabled = false;
                groupStat.Enabled = false;
            }
            else
            {
                dateTimeEnd.Enabled = true;
                dateTimeStart.Enabled = true;
                cbExpried.Enabled = true;
                groupStat.Enabled = true;
            }
        }

        private void btnUnFind_Click(object sender, EventArgs e)
        {
            filter.Clear();
            cbFindBy.SelectedIndex = 0;
            txtFind.Text = "";
            dateTimeStart.Value = DateTime.Now.AddDays(-30);
            dateTimeEnd.Value = DateTime.Now;
            cbExpried.Checked = false;
        }

        private void openBrrowForm_Click(object sender, EventArgs e)
        {
            OrderDetails orderDetails = new OrderDetails();
            orderDetails.ShowDialog();
            changeFollowData();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dataListOrder.SelectedRows.Count < 1)
            {
                MessageBox.Show("Vui lòng chọn đơn sách mà bạn cần sửa");
            }else if (dataListOrder.SelectedRows.Count > 1)
            {
                MessageBox.Show("Để sửa vui lòng chọn chỉ một đơn sách");
            }
            else
            {
                OrderDetails orderDetails = new OrderDetails(Convert.ToInt32(dataListOrder.SelectedRows[0].Cells["id"].Value));
                orderDetails.ShowDialog();
                changeFollowData();
            }
        }

        private void btnReceive_Click(object sender, EventArgs e)
        {
            if (dataListOrder.SelectedRows.Count < 1)
            {
                MessageBox.Show("Vui lòng chọn đơn sách mà người mượn chưa trả");
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Bạn có chắc " + dataListOrder.SelectedRows.Count + " người này đã trả sách?", "Cảnh báo", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    String[] id = new string[dataListOrder.SelectedRows.Count];
                    int i = 0;
                    foreach (DataGridViewRow rows in dataListOrder.SelectedRows)
                    {

                        id[i] = rows.Cells["id"].Value.ToString();
                        i++;
                    }
                    dalBookOrder.updateDateReturn(DateTime.Now,id);
                    changeFollowData();
                }
            }
        }

        private void btnNotReceive_Click(object sender, EventArgs e)
        {
            if (dataListOrder.SelectedRows.Count < 1)
            {
                MessageBox.Show("Vui lòng chọn đơn sách mà người mượn đã trả");
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Bạn có chắc " + dataListOrder.SelectedRows.Count + " người này chưa trả sách?", "Cảnh báo", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    String[] id = new string[dataListOrder.SelectedRows.Count];
                    int i = 0;
                    foreach (DataGridViewRow rows in dataListOrder.SelectedRows)
                    {

                        id[i] = rows.Cells["id"].Value.ToString();
                        i++;
                    }
                    dalBookOrder.updateDateNotReturn(id);
                    changeFollowData();
                }
            }

        }
    }
}
