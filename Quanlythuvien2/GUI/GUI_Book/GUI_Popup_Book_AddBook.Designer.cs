﻿namespace LibraryManager.GUI.GUI_Book
{
    partial class GUI_Popup_Book_AddBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.routeAdd = new System.Windows.Forms.Label();
            this.txtNameBook = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNameAuthor = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDescribe = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.dgvTypeBook = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dgvGroupBook = new System.Windows.Forms.DataGridView();
            this.chkClose = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTypeBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroupBook)).BeginInit();
            this.SuspendLayout();
            // 
            // routeAdd
            // 
            this.routeAdd.BackColor = System.Drawing.SystemColors.Control;
            this.routeAdd.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.routeAdd.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.routeAdd.Location = new System.Drawing.Point(127, 48);
            this.routeAdd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.routeAdd.Name = "routeAdd";
            this.routeAdd.Size = new System.Drawing.Size(157, 50);
            this.routeAdd.TabIndex = 25;
            this.routeAdd.Text = "Tên Sách :";
            this.routeAdd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtNameBook
            // 
            this.txtNameBook.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNameBook.Location = new System.Drawing.Point(303, 59);
            this.txtNameBook.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNameBook.Name = "txtNameBook";
            this.txtNameBook.Size = new System.Drawing.Size(283, 30);
            this.txtNameBook.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(127, 111);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 50);
            this.label1.TabIndex = 27;
            this.label1.Text = "Tên Tác Giả :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtNameAuthor
            // 
            this.txtNameAuthor.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNameAuthor.Location = new System.Drawing.Point(303, 122);
            this.txtNameAuthor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNameAuthor.Name = "txtNameAuthor";
            this.txtNameAuthor.Size = new System.Drawing.Size(283, 30);
            this.txtNameAuthor.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.Control;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(655, 48);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 50);
            this.label2.TabIndex = 29;
            this.label2.Text = "Miêu Tả :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDescribe
            // 
            this.txtDescribe.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescribe.Location = new System.Drawing.Point(819, 59);
            this.txtDescribe.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDescribe.Name = "txtDescribe";
            this.txtDescribe.Size = new System.Drawing.Size(283, 30);
            this.txtDescribe.TabIndex = 28;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.Control;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(655, 111);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 50);
            this.label3.TabIndex = 31;
            this.label3.Text = "Số Lượng :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAmount
            // 
            this.txtAmount.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmount.Location = new System.Drawing.Point(819, 122);
            this.txtAmount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(283, 30);
            this.txtAmount.TabIndex = 30;
            // 
            // dgvTypeBook
            // 
            this.dgvTypeBook.AllowUserToAddRows = false;
            this.dgvTypeBook.AllowUserToDeleteRows = false;
            this.dgvTypeBook.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvTypeBook.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTypeBook.Location = new System.Drawing.Point(176, 246);
            this.dgvTypeBook.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvTypeBook.Name = "dgvTypeBook";
            this.dgvTypeBook.Size = new System.Drawing.Size(266, 313);
            this.dgvTypeBook.TabIndex = 32;
            this.dgvTypeBook.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTypeBook_CellContentClick);
            this.dgvTypeBook.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvTypeBook_CurrentCellDirtyStateChanged);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.Control;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(171, 178);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(169, 50);
            this.label4.TabIndex = 33;
            this.label4.Text = "Thể Loại :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(697, 178);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(169, 50);
            this.label5.TabIndex = 35;
            this.label5.Text = "Nhóm :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvGroupBook
            // 
            this.dgvGroupBook.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvGroupBook.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGroupBook.Location = new System.Drawing.Point(703, 246);
            this.dgvGroupBook.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvGroupBook.Name = "dgvGroupBook";
            this.dgvGroupBook.Size = new System.Drawing.Size(266, 313);
            this.dgvGroupBook.TabIndex = 34;
            this.dgvGroupBook.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGroupBook_CellContentClick);
            // 
            // chkClose
            // 
            this.chkClose.AutoSize = true;
            this.chkClose.Checked = true;
            this.chkClose.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkClose.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkClose.Location = new System.Drawing.Point(132, 713);
            this.chkClose.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkClose.Name = "chkClose";
            this.chkClose.Size = new System.Drawing.Size(266, 31);
            this.chkClose.TabIndex = 36;
            this.chkClose.Text = "Đóng ngay sau khi thêm";
            this.chkClose.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.IndianRed;
            this.btnSave.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btnSave.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSave.Location = new System.Drawing.Point(905, 702);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(197, 44);
            this.btnSave.TabIndex = 37;
            this.btnSave.Text = " Thêm ";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // GUI_Popup_Book_AddBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1212, 785);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.chkClose);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dgvGroupBook);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dgvTypeBook);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtAmount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtDescribe);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNameAuthor);
            this.Controls.Add(this.routeAdd);
            this.Controls.Add(this.txtNameBook);
            this.Name = "GUI_Popup_Book_AddBook";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thêm Sách";
            ((System.ComponentModel.ISupportInitialize)(this.dgvTypeBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroupBook)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label routeAdd;
        private System.Windows.Forms.TextBox txtNameBook;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNameAuthor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDescribe;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.DataGridView dgvTypeBook;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dgvGroupBook;
        private System.Windows.Forms.CheckBox chkClose;
        private System.Windows.Forms.Label btnSave;
    }
}