﻿using LibraryManager.DataAccess;
using LibraryManager.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManager.GUI.GUI_Book
{
    public partial class GUI_Popup_Book_AddBook : Form
    {
        public GUI_Popup_Book_AddBook()
        {
            InitializeComponent();
            InnitDataGridViewTypeBook();
            ChangedListBookType();
            InnitDataGridViewGroupBook();
            ChangedListBookGroup();
        }

        public GUI_Book_ GUI_Book_ { get; internal set; }

        private void InnitDataGridViewTypeBook()
        {
            dgvTypeBook.AutoGenerateColumns = false;

            DataGridViewCheckBoxColumn CheckBoxColumn = new DataGridViewCheckBoxColumn();
            CheckBoxColumn.DataPropertyName = "IsChoose";
            CheckBoxColumn.HeaderText = "Chọn";
            CheckBoxColumn.Name = "IsChoose";
            CheckBoxColumn.Width = 70;
            CheckBoxColumn.DefaultCellStyle.Padding = new Padding(20, 1, 20, 1);
            dgvTypeBook.Columns.Add(CheckBoxColumn);

            DataGridViewColumn col = new DataGridViewTextBoxColumn();
            col.DataPropertyName = "id";
            col.Visible = false;
            col.HeaderText = "Mã Thể Loại";
            col.Name = "id";
            dgvTypeBook.Columns.Add(col);

            DataGridViewColumn col2 = new DataGridViewTextBoxColumn();
            col2.DataPropertyName = "name";
            col2.HeaderText = "Tên Thể Loại";
            col2.Name = "colName";
            col2.Width = 200;
            col2.DefaultCellStyle.Padding = new Padding(20, 1, 20, 1);
            col2.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgvTypeBook.Columns.Add(col2);
        }
        private void InnitDataGridViewGroupBook()
        {
            dgvGroupBook.AutoGenerateColumns = false;

            DataGridViewCheckBoxColumn CheckBoxColumn = new DataGridViewCheckBoxColumn();
            CheckBoxColumn.DataPropertyName = "IsChoose";
            CheckBoxColumn.HeaderText = "Chọn";
            CheckBoxColumn.Name = "IsChoose";
            CheckBoxColumn.Width = 70;
            CheckBoxColumn.DefaultCellStyle.Padding = new Padding(20, 1, 20, 1);
            dgvGroupBook.Columns.Add(CheckBoxColumn);

            DataGridViewColumn col = new DataGridViewTextBoxColumn();
            col.DataPropertyName = "id";
            col.Visible = false;
            col.HeaderText = "Mã Nhóm";
            col.Name = "id";
            dgvGroupBook.Columns.Add(col);

            DataGridViewColumn col2 = new DataGridViewTextBoxColumn();
            col2.DataPropertyName = "name";
            col2.HeaderText = "Tên Nhóm";
            col2.Name = "colName";
            col2.Width = 200;
            col2.DefaultCellStyle.Padding = new Padding(20, 1, 20, 1);
            col2.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgvGroupBook.Columns.Add(col2);
        }
        public void ChangedListBookType(string dk = "")
        {
            DAL_BookType dAL_BookType = new DAL_BookType();
            var lists = dAL_BookType.getAllTypeBook(dk);
            dgvTypeBook.DataSource = lists;
        }
        public void ChangedListBookGroup()
        {
            DAL_BookGroup dAL_BookGroup = new DAL_BookGroup();
            var lists = dAL_BookGroup.getAllGroupBook();
            dgvGroupBook.DataSource = lists;
        }
        private void dgvTypeBook_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            var column = senderGrid.Columns[e.ColumnIndex];
            if (column is DataGridViewCheckBoxColumn && e.RowIndex >= 0)
            {
                var btnType = senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                var id = int.Parse(senderGrid.Rows[e.RowIndex].Cells[1].Value.ToString());
            }
        }

        private void dgvGroupBook_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            var column = senderGrid.Columns[e.ColumnIndex];
            if (column is DataGridViewCheckBoxColumn && e.RowIndex >= 0)
            {
                var btnType = senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                var id = int.Parse(senderGrid.Rows[e.RowIndex].Cells[1].Value.ToString());
            }
        }
        private void dgvTypeBook_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvTypeBook.IsCurrentCellDirty)
            {
                dgvTypeBook.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }
        public void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNameBook.Text) || string.IsNullOrEmpty(txtNameBook.Text))
            {
                MessageBox.Show("Tên Sách Không Được Để Trống");
            }
            else if (string.IsNullOrWhiteSpace(txtAmount.Text) || string.IsNullOrEmpty(txtAmount.Text))
            {
                MessageBox.Show("Chưa Nhập Số Lượng Sách");

            }
            else if (!int.TryParse(txtAmount.Text, out int n))
            {
                MessageBox.Show("Số Lượng Sách Phải Nhập Bằng Số");
            }
            else
            {
                //1. add book
                DTO_Book dTO_Book = new DTO_Book();
                dTO_Book.name = txtNameBook.Text;
                dTO_Book.describe = txtDescribe.Text;
                dTO_Book.nameAuthor = txtNameAuthor.Text;
                dTO_Book.amount = int.Parse(txtAmount.Text);
                DAL_Book dAL_Book = new DAL_Book();
                List<DTO_Book> item = dAL_Book.SearchBookByName(txtNameBook.Text);
                if (item.Count > 0)
                {
                    MessageBox.Show("Đã Có Sách", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    int newID = dAL_Book.AddBook(dTO_Book);
                    var rowsType = dgvTypeBook.Rows;
                    int demType = 0;
                    int demGroup = 0;

                    List<int> listIDsType = new List<int>();
                    foreach (DataGridViewRow row in rowsType)
                    {
                        var id = int.Parse(row.Cells["id"].Value.ToString());
                        var IsChoose = (bool)row.Cells["IsChoose"].Value;
                        if (IsChoose)
                        {
                            listIDsType.Add(id);
                            demType++;
                        }
                    }
                    for (int i = 0; i < demType; i++)
                    {
                        dAL_Book.AddBookTypeMap(newID, listIDsType[i]);
                    }
                    demType = 0;

                    //3. Add to Map 2
                    var rowsGroup = dgvGroupBook.Rows;

                    List<int> listIDsGroup = new List<int>();
                    foreach (DataGridViewRow row in rowsGroup)
                    {

                        var id = int.Parse(row.Cells["id"].Value.ToString());
                        var IsChoose = (bool)row.Cells["IsChoose"].Value;
                        if (IsChoose)
                        {
                            listIDsGroup.Add(id);
                            demGroup++;
                        }
                    }
                    for (int i = 0; i < demGroup; i++)
                    {
                        dAL_Book.AddBookGroupMap(newID, listIDsGroup[i]);
                    }
                    demGroup = 0;

                    MessageBox.Show("Thêm Sách Thành Công");
                }
                
                //2. Add to Map 1
                

                if (chkClose.Checked)
                {
                    this.Close();
                }
                else
                {
                    txtNameBook.Text = "";
                    txtDescribe.Text = "";
                    txtNameAuthor.Text = "";
                    txtAmount.Text = "";
                }

            }
        }

        private void btnSave_MouseEnter(object sender, EventArgs e)
        {
            Label label = (Label)(sender);
            label.BackColor = Color.DarkRed;
        }

        private void btnSave_MouseLeave(object sender, EventArgs e)
        {
            Label label = (Label)(sender);
            label.BackColor = Color.IndianRed;
        }
    }
}
