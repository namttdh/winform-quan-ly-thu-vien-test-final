﻿namespace LibraryManager.GUI
{
    partial class GUI_Book_
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbbSeach = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.lbSearch = new System.Windows.Forms.Label();
            this.txtKeyWord = new System.Windows.Forms.TextBox();
            this.routeAdd = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBook = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBook.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbbSeach
            // 
            this.cbbSeach.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbSeach.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbSeach.FormattingEnabled = true;
            this.cbbSeach.Items.AddRange(new object[] {
            "Tên Sách",
            "Nhóm Sách ",
            "Thể Loại"});
            this.cbbSeach.Location = new System.Drawing.Point(225, 202);
            this.cbbSeach.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbbSeach.Name = "cbbSeach";
            this.cbbSeach.Size = new System.Drawing.Size(160, 35);
            this.cbbSeach.TabIndex = 44;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(89, 198);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 39);
            this.label3.TabIndex = 42;
            this.label3.Text = "Tìm Kiếm :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(95, 268);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1227, 457);
            this.dataGridView1.TabIndex = 40;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // lbSearch
            // 
            this.lbSearch.BackColor = System.Drawing.Color.IndianRed;
            this.lbSearch.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbSearch.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSearch.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbSearch.Location = new System.Drawing.Point(667, 201);
            this.lbSearch.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbSearch.Name = "lbSearch";
            this.lbSearch.Size = new System.Drawing.Size(96, 36);
            this.lbSearch.TabIndex = 38;
            this.lbSearch.Text = "Tìm";
            this.lbSearch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbSearch.Click += new System.EventHandler(this.lbSearch_Click);
            this.lbSearch.MouseEnter += new System.EventHandler(this.lbSearch_MouseEnter);
            this.lbSearch.MouseLeave += new System.EventHandler(this.lbSearch_MouseLeave);
            // 
            // txtKeyWord
            // 
            this.txtKeyWord.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKeyWord.Location = new System.Drawing.Point(395, 201);
            this.txtKeyWord.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtKeyWord.Multiline = true;
            this.txtKeyWord.Name = "txtKeyWord";
            this.txtKeyWord.Size = new System.Drawing.Size(263, 35);
            this.txtKeyWord.TabIndex = 37;
            // 
            // routeAdd
            // 
            this.routeAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.routeAdd.BackColor = System.Drawing.Color.IndianRed;
            this.routeAdd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.routeAdd.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.routeAdd.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.routeAdd.Location = new System.Drawing.Point(1164, 194);
            this.routeAdd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.routeAdd.Name = "routeAdd";
            this.routeAdd.Size = new System.Drawing.Size(157, 44);
            this.routeAdd.TabIndex = 36;
            this.routeAdd.Text = "+ Thêm ";
            this.routeAdd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.routeAdd.Click += new System.EventHandler(this.routeAdd_Click);
            this.routeAdd.MouseEnter += new System.EventHandler(this.routeAdd_MouseEnter);
            this.routeAdd.MouseLeave += new System.EventHandler(this.routeAdd_MouseLeave);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.IndianRed;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(16, 11);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(227, 58);
            this.label2.TabIndex = 35;
            this.label2.Text = "Sách";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBook
            // 
            this.groupBook.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBook.Controls.Add(this.label1);
            this.groupBook.Location = new System.Drawing.Point(959, 11);
            this.groupBook.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBook.Name = "groupBook";
            this.groupBook.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBook.Size = new System.Drawing.Size(363, 140);
            this.groupBook.TabIndex = 45;
            this.groupBook.TabStop = false;
            this.groupBook.Text = "Thống Kê";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 37);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 17);
            this.label1.TabIndex = 0;
            // 
            // GUI_Book_
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1423, 754);
            this.Controls.Add(this.groupBook);
            this.Controls.Add(this.cbbSeach);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lbSearch);
            this.Controls.Add(this.txtKeyWord);
            this.Controls.Add(this.routeAdd);
            this.Controls.Add(this.label2);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "GUI_Book_";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GUI_Book_";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBook.ResumeLayout(false);
            this.groupBook.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cbbSeach;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lbSearch;
        private System.Windows.Forms.TextBox txtKeyWord;
        private System.Windows.Forms.Label routeAdd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBook;
        private System.Windows.Forms.Label label1;
    }
}