﻿using LibraryManager.DataAccess;
using LibraryManager.DTO;
using LibraryManager.DTO.Opject;
using LibraryManager.GUI.GUI_Book;
using Quanlythuvien2.GUI.GUI_Book;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManager.GUI
{
    public partial class GUI_Book_ : Form
    {
        public GUI_Book_()
        {
            InitializeComponent();
            cbbSeach.SelectedIndex = 0;
            InnitDataGridView();
            ChangedListBook();
        }

        private void InnitDataGridView()
        {
            dataGridView1.AutoGenerateColumns = false;

            DataGridViewButtonColumn buttonColumnXoa = new DataGridViewButtonColumn();
            buttonColumnXoa.DataPropertyName = "id";
            buttonColumnXoa.HeaderText = "";
            buttonColumnXoa.Name = "btnXoa";
            buttonColumnXoa.Text = "Xóa";
            buttonColumnXoa.Width = 50;
            buttonColumnXoa.MinimumWidth = 50;
            buttonColumnXoa.DefaultCellStyle.BackColor = Color.Red;
            buttonColumnXoa.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(buttonColumnXoa);

            DataGridViewButtonColumn buttonColumnSua = new DataGridViewButtonColumn();
            buttonColumnSua.DataPropertyName = "id";
            buttonColumnSua.HeaderText = "";
            buttonColumnSua.Name = "btnSua";
            buttonColumnSua.Text = "Sửa";
            buttonColumnSua.Width = 50;
            buttonColumnSua.MinimumWidth = 50;
            buttonColumnSua.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(buttonColumnSua);

            DataGridViewColumn col = new DataGridViewTextBoxColumn();
            col.DataPropertyName = "id";
            col.HeaderText = "Mã Sách";
            col.Name = "colId";
            col.Width = 50;
            dataGridView1.Columns.Add(col);

            DataGridViewColumn col2 = new DataGridViewTextBoxColumn();
            col2.DataPropertyName = "name";
            col2.HeaderText = "Tên Sách";
            col2.Name = "colName";
            col2.MinimumWidth = 100;
            col2.DefaultCellStyle.Padding = new Padding(20, 1, 20, 1);
            dataGridView1.Columns.Add(col2);

            DataGridViewColumn col3 = new DataGridViewTextBoxColumn();
            col3.DataPropertyName = "describe";
            col3.HeaderText = "Nội Dung";
            col3.Name = "colDescribe";
            col3.MinimumWidth = 100;
            //col3.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            col3.DefaultCellStyle.Padding = new Padding(20, 1, 20, 1);
            dataGridView1.Columns.Add(col3);

            DataGridViewColumn col4 = new DataGridViewTextBoxColumn();
            col4.DataPropertyName = "nameAuthor";
            col4.HeaderText = "Tên Tác Giả";
            col4.Name = "colNameAuthor";
            col4.MinimumWidth = 50;
            col4.DefaultCellStyle.Padding = new Padding(20, 1, 20, 1);
            dataGridView1.Columns.Add(col4);

            DataGridViewColumn col5 = new DataGridViewTextBoxColumn();
            col5.DataPropertyName = "amount";
            col5.HeaderText = "Số Lượng";
            col5.Name = "colAmount";
            col5.MinimumWidth = 50;
            col5.DefaultCellStyle.Padding = new Padding(20, 1, 20, 1);
            dataGridView1.Columns.Add(col5);

            DataGridViewColumn col6 = new DataGridViewTextBoxColumn();
            col6.DataPropertyName = "nameType";
            col6.HeaderText = "Thể Loại";
            col6.Name = "colBookType";
            col6.MinimumWidth = 100;
            col6.DefaultCellStyle.Padding = new Padding(20, 1, 20, 1);
            dataGridView1.Columns.Add(col6);
            //dataGridView1.Columns.Add("colBookType", "Thể Loại");
            //dataGridView1.Columns["colBookType"].DefaultCellStyle.Padding = new Padding(20, 1, 20, 1);


            DataGridViewColumn col7 = new DataGridViewTextBoxColumn();
            col6.DataPropertyName = "nameGroup";
            col7.HeaderText = "Nhóm";
            col7.Name = "colBookGroup";
            col7.MinimumWidth = 200;
            col7.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            col7.DefaultCellStyle.Padding = new Padding(20, 1, 20, 1);
            dataGridView1.Columns.Add(col7);

        }
        private void ChangedListBook(string dk = "")
        {
            dataGridView1.Rows.Clear();
            DAL_Book dAL_Book = new DAL_Book();
            List<BookTypeMap> lists = dAL_Book.getAllBook(dk);
            foreach (BookTypeMap items in lists)
            {
                dataGridView1.Rows.Add(items.id, items.id, items.id, items.name, items.describe, items.nameAuthor, items.amount, items.nameType, items.nameGroup);
            }
            //dataGridView1.DataSource = lists;
        }

        private void routeAdd_Click(object sender, EventArgs e)
        {
            string dk = "";
            GUI_Popup_Book_AddBook gUI_Popup_Book_AddBook = new GUI_Popup_Book_AddBook();
            gUI_Popup_Book_AddBook.GUI_Book_ = this;
            gUI_Popup_Book_AddBook.ShowDialog();
            ChangedListBook(dk);
        }
        private void lbSearch_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            string dk = "";
            if (cbbSeach.SelectedIndex == 0)
            {
                dk = " Where bk.name like N'%" + txtKeyWord.Text + "%'";
            }
            else if (cbbSeach.SelectedIndex == 2)
            {
                dk = " Where bk.nameType like N'%" + txtKeyWord.Text + "%'";
            }
            else if (cbbSeach.SelectedIndex == 1)
            {
                dk = " Where bk.nameGroup like N'%" + txtKeyWord.Text + "%'";
            }
            ChangedListBook(dk);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            var column = senderGrid.Columns[e.ColumnIndex];

            if (column is DataGridViewButtonColumn && e.RowIndex >= 0)
            {

                var btnBook = senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                var id = int.Parse(senderGrid.Rows[e.RowIndex].Cells[2].Value.ToString());

                if (btnBook == "Xóa")
                {
                    DialogResult dlr = MessageBox.Show("Hãy Chắc Chắn Bạn Muốn Xóa", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (dlr == DialogResult.Yes)
                    {
                        DAL_Book dAL_Book = new DAL_Book();
                        var isDelete1 = dAL_Book.DeleteBookTypeMap(id);
                        if (isDelete1)
                        {
                            var isDelete2 = dAL_Book.DeleteBookGroupMap(id);
                            if (isDelete2)
                            {
                                var isDelete3 = dAL_Book.DeleteBook(id);
                                if (isDelete3)
                                {
                                    ChangedListBook();
                                }
                            }
                        }
                    }
                }
                else if (btnBook == "Sửa")
                {
                    GUI_PopupEditBook gUI_PopupEditBook = new GUI_PopupEditBook(id);
                    gUI_PopupEditBook.ShowDialog();
                    if (gUI_PopupEditBook.IsUpdate)
                    {
                        ChangedListBook();
                    }
                }
            }
        }

        private void routeAdd_MouseEnter(object sender, EventArgs e)
        {
            Label label = (Label)(sender);
            label.BackColor = Color.DarkRed;
        }

        private void routeAdd_MouseLeave(object sender, EventArgs e)
        {
            Label label = (Label)(sender);
            label.BackColor = Color.IndianRed;
        }

        private void lbSearch_MouseEnter(object sender, EventArgs e)
        {
            Label label = (Label)(sender);
            label.BackColor = Color.DarkRed;
        }

        private void lbSearch_MouseLeave(object sender, EventArgs e)
        {
            Label label = (Label)(sender);
            label.BackColor = Color.IndianRed;
        }
    }
}
