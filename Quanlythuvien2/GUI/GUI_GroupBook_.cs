﻿using LibraryManager.DataAccess;
using LibraryManager.GUI.GUI_GroupBook;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManager.GUI
{
    public partial class GUI_GroupBook_ : Form
    {
        public GUI_GroupBook_()
        {
            InitializeComponent();
            InnitDataGridView();
            ChangedListBookGroup();
        }
        private void InnitDataGridView()
        {
            dataGridView1.AutoGenerateColumns = false;

            DataGridViewButtonColumn buttonColumnXoa = new DataGridViewButtonColumn();
            buttonColumnXoa.DataPropertyName = "id";
            buttonColumnXoa.HeaderText = "";
            buttonColumnXoa.Name = "Status Request";
            buttonColumnXoa.Text = "Xóa";
            buttonColumnXoa.Width = 50;
            buttonColumnXoa.DefaultCellStyle.BackColor = Color.Red;
            buttonColumnXoa.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(buttonColumnXoa);

            DataGridViewButtonColumn buttonColumnSua = new DataGridViewButtonColumn();
            buttonColumnSua.DataPropertyName = "id";
            buttonColumnSua.HeaderText = "";
            buttonColumnSua.Name = "btnSua";
            buttonColumnSua.Text = "Sửa";
            buttonColumnSua.Width = 50;
            buttonColumnSua.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(buttonColumnSua);

            DataGridViewColumn col = new DataGridViewTextBoxColumn();
            col.DataPropertyName = "id";
            col.HeaderText = "Mã Nhóm";
            col.Name = "colId";
            dataGridView1.Columns.Add(col);

            DataGridViewColumn col2 = new DataGridViewTextBoxColumn();
            col2.DataPropertyName = "name";
            col2.HeaderText = "Tên Nhóm";
            col2.Name = "colName";
            col2.Width = 200;
            col2.DefaultCellStyle.Padding = new Padding(20, 1, 20, 1);
            dataGridView1.Columns.Add(col2);

            DataGridViewColumn col3 = new DataGridViewTextBoxColumn();
            col3.DataPropertyName = "note";
            col3.HeaderText = "Ghi Chú";
            col3.Name = "colNote";
            col3.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            col3.DefaultCellStyle.Padding = new Padding(20, 1, 20, 1);
            dataGridView1.Columns.Add(col3);

            //foreach (DataGridViewColumn column_i in dataGridView1.Columns)
            //{
            //    column_i.SortMode = DataGridViewColumnSortMode.Automatic;
            //}
        }
        public void ChangedListBookGroup()
        {
            DAL_BookGroup dAL_BookGroup = new DAL_BookGroup();
            var lists = dAL_BookGroup.SearchBookGroupByName(txtKeyWord.Text);
            dataGridView1.DataSource = lists;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            var column = senderGrid.Columns[e.ColumnIndex];
            var btnType = senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            //var id = int.Parse(senderGrid.Rows[e.RowIndex].Cells[2].Value.ToString());
            var selectedRow = dataGridView1.SelectedRows[0];
            var id = int.Parse(selectedRow.Cells[2].Value.ToString());
            if (btnType == "Xóa")
            {

                DialogResult dlr = MessageBox.Show("Hãy Chắc Chắn Bạn Muốn Xóa", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (dlr == DialogResult.Yes)
                {
                    DAL_BookGroup dAL_BookGroup = new DAL_BookGroup();
                    var isDeleted = dAL_BookGroup.DeleteBookGroup(id);
                    if (isDeleted)
                    {
                        ChangedListBookGroup();
                    }
                }

            }
            else if (btnType == "Sửa")
            {
                GUI_PopupEditGroupBook gUI_PopupEditGroupBook = new GUI_PopupEditGroupBook(id);
                gUI_PopupEditGroupBook.ShowDialog();
                if (gUI_PopupEditGroupBook.IsUpdate)
                {
                    ChangedListBookGroup();
                }
            }
        }

        private void routeAdd_Click(object sender, EventArgs e)
        {
            GUI_Popup_GroupBook_AddGroup gUI_Popup_GroupBook_AddGroup = new GUI_Popup_GroupBook_AddGroup();
            gUI_Popup_GroupBook_AddGroup.GUI_GroupBook_ = this;
            gUI_Popup_GroupBook_AddGroup.ShowDialog();
        }
        private void lbSearch_Click(object sender, EventArgs e)
        {
            ChangedListBookGroup();
        }


        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            var selectedRow = dataGridView1.SelectedRows[0];
            var id = int.Parse(selectedRow.Cells[2].Value.ToString());

            GUI_PopupEditGroupBook GUI_PopupEditGroupBook = new GUI_PopupEditGroupBook(id);
            GUI_PopupEditGroupBook.ShowDialog();
        }

        private void routeAdd_MouseEnter(object sender, EventArgs e)
        {
            Label label = (Label)(sender);
            label.BackColor = Color.DarkRed;
        }

        private void routeAdd_MouseLeave(object sender, EventArgs e)
        {
            Label label = (Label)(sender);
            label.BackColor = Color.IndianRed;
        }

        private void lbSearch_MouseEnter(object sender, EventArgs e)
        {
            Label label = (Label)(sender);
            label.BackColor = Color.DarkRed;
        }

        private void lbSearch_MouseLeave(object sender, EventArgs e)
        {
            Label label = (Label)(sender);
            label.BackColor = Color.IndianRed;
        }
    }
}
