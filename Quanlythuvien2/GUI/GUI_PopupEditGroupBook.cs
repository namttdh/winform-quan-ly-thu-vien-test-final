﻿using LibraryManager.DataAccess;
using LibraryManager.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManager.GUI.GUI_GroupBook
{
    public partial class GUI_PopupEditGroupBook : Form
    {
        int ID;
        public GUI_PopupEditGroupBook(int id)
        {
            InitializeComponent();

            ID = id;
            DAL_BookGroup dAL_BookGroup = new DAL_BookGroup();
            var item = dAL_BookGroup.GetBookGroupByID(id);
            txtID.Text = item.id.ToString();
            txtName.Text = item.name;
            txtNote.Text = item.note;
        }

        public bool IsUpdate = false;

        private void btnSave_Click(object sender, EventArgs e)
        {
            
            if (string.IsNullOrWhiteSpace(txtName.Text) || string.IsNullOrEmpty(txtName.Text))
            {
                MessageBox.Show("Tên Không Được Để Trống");
            }
            else
            {

                DTO_BookGroup dTO_BookGroup = new DTO_BookGroup();
                dTO_BookGroup.id = ID;
                dTO_BookGroup.name = txtName.Text;
                dTO_BookGroup.note = txtNote.Text;
                DAL_BookGroup dAL_BookGroup = new DAL_BookGroup();
                List<DTO_BookGroup> item = dAL_BookGroup.SearchBookGroupByName(txtName.Text);
                if (item.Count > 0)
                {
                    MessageBox.Show("Đã Có Nhóm", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    dAL_BookGroup.UpdateBookGroup(dTO_BookGroup);
                }
                
                IsUpdate = true;
                this.Close();
            }
           
        }
    }
}
