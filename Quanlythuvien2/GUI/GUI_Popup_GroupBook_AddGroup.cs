﻿using LibraryManager.DataAccess;
using LibraryManager.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManager.GUI.GUI_GroupBook
{
    public partial class GUI_Popup_GroupBook_AddGroup : Form
    {
        public GUI_Popup_GroupBook_AddGroup()
        {
            InitializeComponent();

        }

        public GUI_GroupBook_ GUI_GroupBook_ { get; internal set; }

        private void btnSave_Click(object sender, EventArgs e)
        {


            if (string.IsNullOrWhiteSpace(txtNameGroup.Text) || string.IsNullOrEmpty(txtNameGroup.Text))
            {
                MessageBox.Show("Tên Không Được Để Trống");
            }
            else
            {
                DTO_BookGroup dTO_BookGroup = new DTO_BookGroup();
                dTO_BookGroup.name = txtNameGroup.Text;
                dTO_BookGroup.note = txtNote.Text;
                DAL_BookGroup dAL_BookGroup = new DAL_BookGroup();
                List<DTO_BookGroup> item = dAL_BookGroup.SearchBookGroupByName(txtNameGroup.Text);
                if (item.Count > 0)
                {
                    MessageBox.Show("Đã Có Nhóm", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    dAL_BookGroup.AddBookGroup(dTO_BookGroup);
                }

                if (chkClose.Checked)
                {
                    this.Close();
                }
                else
                {
                    txtNameGroup.Text = "";
                    txtNote.Text = "";
                }

                GUI_GroupBook_.ChangedListBookGroup();
            }

        }
    }
}
