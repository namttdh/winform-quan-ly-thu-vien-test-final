﻿using LibraryManager.DataAccess;
using LibraryManager.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManager.GUI.GUI_TypeBook
{
    public partial class GUI_PopupEditTypeBook : Form
    {
        int ID;
        public GUI_PopupEditTypeBook(int id)
        {
            InitializeComponent();
            ID = id;
            DAL_BookType dAL_BookType = new DAL_BookType();
            var item = dAL_BookType.GetBookTypeByID(id);
            txtID.Text = item.id.ToString();
            txtName.Text = item.name;
            txtNote.Text = item.note;
        }
        public bool IsUpdate = false;
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtName.Text) || string.IsNullOrEmpty(txtName.Text))
            {
                MessageBox.Show("Tên Không Được Để Trống");
            }
            else
            {

                DTO_BookType dTO_BookType = new DTO_BookType
                {
                    id = ID,
                    name = txtName.Text,
                    note = txtNote.Text
                };
                DAL_BookType dAL_BookType = new DAL_BookType();
                List<DTO_BookType> item = dAL_BookType.SearchBookTypeByName(txtName.Text);
                if (item.Count > 0)
                {
                    MessageBox.Show("Đã Có Nhóm", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    dAL_BookType.UpdateBookType(dTO_BookType);
                }
                IsUpdate = true;
                this.Close();
            }

        }

        private void btnSave_MouseEnter(object sender, EventArgs e)
        {
            Label label = (Label)(sender);
            label.BackColor = Color.DarkRed;
        }

        private void btnSave_MouseLeave(object sender, EventArgs e)
        {
            Label label = (Label)(sender);
            label.BackColor = Color.IndianRed;
        }
    }
}
