﻿namespace LibraryManager.GUI.GUI_TypeBook
{
    partial class GUI_Popup_TypeBook_AddType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Label();
            this.chkClose = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.routeAdd = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.RichTextBox();
            this.txtNameType = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.IndianRed;
            this.btnSave.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btnSave.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSave.Location = new System.Drawing.Point(612, 350);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(197, 44);
            this.btnSave.TabIndex = 26;
            this.btnSave.Text = " Thêm ";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.MouseEnter += new System.EventHandler(this.btnSave_MouseEnter);
            this.btnSave.MouseLeave += new System.EventHandler(this.btnSave_MouseLeave);
            // 
            // chkClose
            // 
            this.chkClose.AutoSize = true;
            this.chkClose.Checked = true;
            this.chkClose.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkClose.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkClose.Location = new System.Drawing.Point(104, 350);
            this.chkClose.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkClose.Name = "chkClose";
            this.chkClose.Size = new System.Drawing.Size(266, 31);
            this.chkClose.TabIndex = 25;
            this.chkClose.Text = "Đóng ngay sau khi thêm";
            this.chkClose.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.Control;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(87, 130);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 50);
            this.label2.TabIndex = 24;
            this.label2.Text = "Ghi Chú";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // routeAdd
            // 
            this.routeAdd.BackColor = System.Drawing.SystemColors.Control;
            this.routeAdd.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.routeAdd.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.routeAdd.Location = new System.Drawing.Point(87, 70);
            this.routeAdd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.routeAdd.Name = "routeAdd";
            this.routeAdd.Size = new System.Drawing.Size(157, 50);
            this.routeAdd.TabIndex = 23;
            this.routeAdd.Text = "Thể Loại ";
            this.routeAdd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNote
            // 
            this.txtNote.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNote.Location = new System.Drawing.Point(285, 146);
            this.txtNote.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(523, 160);
            this.txtNote.TabIndex = 22;
            this.txtNote.Text = "";
            // 
            // txtNameType
            // 
            this.txtNameType.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNameType.Location = new System.Drawing.Point(285, 81);
            this.txtNameType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNameType.Name = "txtNameType";
            this.txtNameType.Size = new System.Drawing.Size(523, 30);
            this.txtNameType.TabIndex = 21;
            // 
            // GUI_Popup_TypeBook_AddType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 481);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.chkClose);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.routeAdd);
            this.Controls.Add(this.txtNote);
            this.Controls.Add(this.txtNameType);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "GUI_Popup_TypeBook_AddType";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thêm Loại  Sách";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label btnSave;
        private System.Windows.Forms.CheckBox chkClose;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label routeAdd;
        private System.Windows.Forms.RichTextBox txtNote;
        private System.Windows.Forms.TextBox txtNameType;
    }
}