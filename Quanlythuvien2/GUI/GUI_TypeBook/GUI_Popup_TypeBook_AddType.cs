﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryManager.DataAccess;
using LibraryManager.DTO;

namespace LibraryManager.GUI.GUI_TypeBook
{
    public partial class GUI_Popup_TypeBook_AddType : Form
    {

        public GUI_Popup_TypeBook_AddType()
        {
            InitializeComponent();
        }

        public GUI_TypeBook_ GUI_TypeBook_ { get; internal set; }

        private void btnSave_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrWhiteSpace(txtNameType.Text) || string.IsNullOrEmpty(txtNameType.Text))
            {
                MessageBox.Show("Tên Không Được Để Trống");
            }
            else
            {
                DTO_BookType dTO_BookType = new DTO_BookType();
                dTO_BookType.name = txtNameType.Text;
                dTO_BookType.note = txtNote.Text;
                DAL_BookType dAL_BookType = new DAL_BookType();
                List<DTO_BookType> item = dAL_BookType.SearchBookTypeByName(txtNameType.Text);
                if (item.Count > 0)
                {
                    MessageBox.Show("Đã Có Thể Loại", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    dAL_BookType.AddBookType(dTO_BookType);
                }

                if (chkClose.Checked)
                {
                    this.Close();
                }
                else
                {
                    txtNameType.Text = "";
                    txtNote.Text = "";
                }
                GUI_TypeBook_.ChangedListBookType();
            }

        }

        private void btnSave_MouseEnter(object sender, EventArgs e)
        {
            Label label = (Label)(sender);
            label.BackColor = Color.DarkRed;
        }

        private void btnSave_MouseLeave(object sender, EventArgs e)
        {
            Label label = (Label)(sender);
            label.BackColor = Color.IndianRed;
        }
    }
}
