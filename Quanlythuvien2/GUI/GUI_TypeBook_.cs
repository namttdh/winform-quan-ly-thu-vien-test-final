﻿using LibraryManager.DataAccess;
using LibraryManager.GUI.GUI_TypeBook;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManager.GUI
{
    public partial class GUI_TypeBook_ : Form
    {
        public GUI_TypeBook_()
        {
            InitializeComponent();
            InnitDataGridView();
            ChangedListBookType();
        }
        public void ChangedListBookType()
        {
            DAL_BookType dAL_BookType = new DAL_BookType();
            var lists = dAL_BookType.SearchBookTypeByName(txtKeyWord.Text);
            dataGridView1.DataSource = lists;
        }

        private void InnitDataGridView()
        {
            dataGridView1.AutoGenerateColumns = false;

            DataGridViewButtonColumn buttonColumnXoa = new DataGridViewButtonColumn();
            buttonColumnXoa.DataPropertyName = "id";
            buttonColumnXoa.HeaderText = "";
            buttonColumnXoa.Name = "btnXoa";
            buttonColumnXoa.Text = "Xóa";
            buttonColumnXoa.Width = 50;
            buttonColumnXoa.DefaultCellStyle.BackColor = Color.Red;
            buttonColumnXoa.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(buttonColumnXoa);

            DataGridViewButtonColumn buttonColumnSua = new DataGridViewButtonColumn();
            buttonColumnSua.DataPropertyName = "id";
            buttonColumnSua.HeaderText = "";
            buttonColumnSua.Name = "btnSua";
            buttonColumnSua.Text = "Sửa";
            buttonColumnSua.Width = 50;
            buttonColumnSua.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(buttonColumnSua);

            DataGridViewColumn col = new DataGridViewTextBoxColumn();
            col.DataPropertyName = "id";
            col.HeaderText = "Mã Thể Loại";
            col.Name = "id";
            dataGridView1.Columns.Add(col);

            DataGridViewColumn col2 = new DataGridViewTextBoxColumn();
            col2.DataPropertyName = "name";
            col2.HeaderText = "Tên Thể Loại";
            col2.Name = "colName";
            col2.Width = 200;
            col2.DefaultCellStyle.Padding = new Padding(20, 1, 20, 1);
            dataGridView1.Columns.Add(col2);

            DataGridViewColumn col3 = new DataGridViewTextBoxColumn();
            col3.DataPropertyName = "note";
            col3.HeaderText = "Ghi Chú";
            col3.Name = "colNote";
            col3.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            col3.DefaultCellStyle.Padding = new Padding(20, 1, 20, 1);
            dataGridView1.Columns.Add(col3);

        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            var column = senderGrid.Columns[e.ColumnIndex];

            if (column is DataGridViewButtonColumn && e.RowIndex >= 0)
            {

                var btnType = senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                var id = int.Parse(senderGrid.Rows[e.RowIndex].Cells[2].Value.ToString());

                if (btnType == "Xóa")
                {

                    DialogResult dlr = MessageBox.Show("Hãy Chắc Chắn Bạn Muốn Xóa", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (dlr == DialogResult.Yes)
                    {
                        DAL_BookType dAL_BookType = new DAL_BookType();
                        var isDelete = dAL_BookType.DeleteBookType(id);
                        if (isDelete)
                        {
                            ChangedListBookType();
                        }
                    }

                }
                else if (btnType == "Sửa")
                {
                    GUI_PopupEditTypeBook gUI_PopupEditTypeBook = new GUI_PopupEditTypeBook(id);
                    gUI_PopupEditTypeBook.ShowDialog();
                    if (gUI_PopupEditTypeBook.IsUpdate)
                    {
                        ChangedListBookType();
                    }
                }

            }

        }

        private void routeAdd_Click(object sender, EventArgs e)
        {
            GUI_Popup_TypeBook_AddType gUI_Popup_TypeBook_AddType = new GUI_Popup_TypeBook_AddType();
            gUI_Popup_TypeBook_AddType.GUI_TypeBook_ = this;
            gUI_Popup_TypeBook_AddType.ShowDialog();
        }

        private void lbSearch_Click(object sender, EventArgs e)
        {
            ChangedListBookType();
        }

        private void dataGridView1_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dataGridView1.IsCurrentCellDirty)
            {
                dataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            var selectedRow = dataGridView1.SelectedRows[0];
            var id = int.Parse(selectedRow.Cells[2].Value.ToString());

            GUI_PopupEditTypeBook GUI_PopupEditTypeBook = new GUI_PopupEditTypeBook(id);
            GUI_PopupEditTypeBook.ShowDialog();
        }

        private void routeAdd_MouseEnter(object sender, EventArgs e)
        {
            Label label = (Label)(sender);
            label.BackColor = Color.DarkRed;
        }

        private void routeAdd_MouseLeave(object sender, EventArgs e)
        {
            Label label = (Label)(sender);
            label.BackColor = Color.IndianRed;
        }

        private void lbSearch_MouseEnter(object sender, EventArgs e)
        {
            Label label = (Label)(sender);
            label.BackColor = Color.DarkRed;
        }

        private void lbSearch_MouseLeave(object sender, EventArgs e)
        {
            Label label = (Label)(sender);
            label.BackColor = Color.IndianRed;
        }

        
    }
}
