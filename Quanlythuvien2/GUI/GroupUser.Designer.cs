﻿namespace Quanlythuvien2.GUI
{
    partial class GroupUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbCount = new System.Windows.Forms.Label();
            this.lbGrName = new System.Windows.Forms.Label();
            this.lbId = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.grbInfo = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.grbUser = new System.Windows.Forms.GroupBox();
            this.listView = new System.Windows.Forms.ListView();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lbRoleCount = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.grbDetail = new System.Windows.Forms.GroupBox();
            this.lbAllUser = new System.Windows.Forms.Label();
            this.btnFind = new System.Windows.Forms.Button();
            this.tbFind = new System.Windows.Forms.TextBox();
            this.cbbFind = new System.Windows.Forms.ComboBox();
            this.grbCRUD = new System.Windows.Forms.GroupBox();
            this.btnViewAllUser = new System.Windows.Forms.Button();
            this.grbFind = new System.Windows.Forms.GroupBox();
            this.sqlConnection = new System.Data.SqlClient.SqlConnection();
            this.grbInfo.SuspendLayout();
            this.grbUser.SuspendLayout();
            this.grbDetail.SuspendLayout();
            this.grbCRUD.SuspendLayout();
            this.grbFind.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbCount
            // 
            this.lbCount.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbCount.AutoSize = true;
            this.lbCount.Location = new System.Drawing.Point(166, 93);
            this.lbCount.Name = "lbCount";
            this.lbCount.Size = new System.Drawing.Size(32, 17);
            this.lbCount.TabIndex = 7;
            this.lbCount.Text = "100";
            // 
            // lbGrName
            // 
            this.lbGrName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbGrName.AutoSize = true;
            this.lbGrName.Location = new System.Drawing.Point(167, 58);
            this.lbGrName.Name = "lbGrName";
            this.lbGrName.Size = new System.Drawing.Size(59, 17);
            this.lbGrName.TabIndex = 6;
            this.lbGrName.Text = "Quản trị";
            // 
            // lbId
            // 
            this.lbId.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbId.AutoSize = true;
            this.lbId.Location = new System.Drawing.Point(167, 22);
            this.lbId.Name = "lbId";
            this.lbId.Size = new System.Drawing.Size(16, 17);
            this.lbId.TabIndex = 5;
            this.lbId.Text = "1";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Số lượng thành viên:";
            // 
            // label
            // 
            this.label.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(7, 22);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(25, 17);
            this.label.TabIndex = 0;
            this.label.Text = "ID:";
            // 
            // grbInfo
            // 
            this.grbInfo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.grbInfo.Controls.Add(this.lbCount);
            this.grbInfo.Controls.Add(this.lbGrName);
            this.grbInfo.Controls.Add(this.lbId);
            this.grbInfo.Controls.Add(this.label4);
            this.grbInfo.Controls.Add(this.label2);
            this.grbInfo.Controls.Add(this.label);
            this.grbInfo.Location = new System.Drawing.Point(693, 330);
            this.grbInfo.Name = "grbInfo";
            this.grbInfo.Size = new System.Drawing.Size(330, 206);
            this.grbInfo.TabIndex = 12;
            this.grbInfo.TabStop = false;
            this.grbInfo.Text = "Thông tin nhóm";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tên nhóm:";
            // 
            // grbUser
            // 
            this.grbUser.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.grbUser.Controls.Add(this.listView);
            this.grbUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbUser.Location = new System.Drawing.Point(15, 144);
            this.grbUser.Name = "grbUser";
            this.grbUser.Size = new System.Drawing.Size(672, 392);
            this.grbUser.TabIndex = 10;
            this.grbUser.TabStop = false;
            this.grbUser.Text = "Danh sách nhóm";
            // 
            // listView
            // 
            this.listView.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.listView.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView.FullRowSelect = true;
            this.listView.HideSelection = false;
            this.listView.Location = new System.Drawing.Point(7, 21);
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(641, 365);
            this.listView.TabIndex = 0;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listView_MouseClick);
            this.listView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listView_MouseDoubleClick);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(10, 100);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(160, 61);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "Xoá";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lbRoleCount
            // 
            this.lbRoleCount.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbRoleCount.AutoSize = true;
            this.lbRoleCount.Location = new System.Drawing.Point(156, 28);
            this.lbRoleCount.Name = "lbRoleCount";
            this.lbRoleCount.Size = new System.Drawing.Size(35, 18);
            this.lbRoleCount.TabIndex = 1;
            this.lbRoleCount.Text = "100";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(170, 33);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(160, 61);
            this.btnUpdate.TabIndex = 1;
            this.btnUpdate.Text = "Sửa";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(10, 33);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(160, 61);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // grbDetail
            // 
            this.grbDetail.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.grbDetail.Controls.Add(this.lbRoleCount);
            this.grbDetail.Controls.Add(this.lbAllUser);
            this.grbDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbDetail.Location = new System.Drawing.Point(694, 40);
            this.grbDetail.Name = "grbDetail";
            this.grbDetail.Size = new System.Drawing.Size(336, 102);
            this.grbDetail.TabIndex = 9;
            this.grbDetail.TabStop = false;
            this.grbDetail.Text = "Thống kê nhóm";
            // 
            // lbAllUser
            // 
            this.lbAllUser.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbAllUser.AutoSize = true;
            this.lbAllUser.Location = new System.Drawing.Point(7, 28);
            this.lbAllUser.Name = "lbAllUser";
            this.lbAllUser.Size = new System.Drawing.Size(127, 18);
            this.lbAllUser.TabIndex = 0;
            this.lbAllUser.Text = "Tổng số nhóm: ";
            // 
            // btnFind
            // 
            this.btnFind.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFind.Location = new System.Drawing.Point(394, 62);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(75, 28);
            this.btnFind.TabIndex = 2;
            this.btnFind.Text = "Tìm kiếm";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // tbFind
            // 
            this.tbFind.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbFind.Location = new System.Drawing.Point(191, 25);
            this.tbFind.Name = "tbFind";
            this.tbFind.Size = new System.Drawing.Size(457, 24);
            this.tbFind.TabIndex = 1;
            // 
            // cbbFind
            // 
            this.cbbFind.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbbFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbFind.FormattingEnabled = true;
            this.cbbFind.Items.AddRange(new object[] {
            "Tên nhóm",
            "ID nhóm"});
            this.cbbFind.Location = new System.Drawing.Point(7, 23);
            this.cbbFind.Name = "cbbFind";
            this.cbbFind.Size = new System.Drawing.Size(153, 26);
            this.cbbFind.TabIndex = 0;
            // 
            // grbCRUD
            // 
            this.grbCRUD.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.grbCRUD.Controls.Add(this.btnDelete);
            this.grbCRUD.Controls.Add(this.btnViewAllUser);
            this.grbCRUD.Controls.Add(this.btnUpdate);
            this.grbCRUD.Controls.Add(this.btnAdd);
            this.grbCRUD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbCRUD.Location = new System.Drawing.Point(693, 148);
            this.grbCRUD.Name = "grbCRUD";
            this.grbCRUD.Size = new System.Drawing.Size(336, 175);
            this.grbCRUD.TabIndex = 11;
            this.grbCRUD.TabStop = false;
            this.grbCRUD.Text = "Quản lý nhóm";
            // 
            // btnViewAllUser
            // 
            this.btnViewAllUser.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnViewAllUser.AutoSize = true;
            this.btnViewAllUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewAllUser.Location = new System.Drawing.Point(170, 100);
            this.btnViewAllUser.Name = "btnViewAllUser";
            this.btnViewAllUser.Size = new System.Drawing.Size(160, 61);
            this.btnViewAllUser.TabIndex = 1;
            this.btnViewAllUser.Text = "Tất cả nhóm";
            this.btnViewAllUser.UseVisualStyleBackColor = true;
            this.btnViewAllUser.Click += new System.EventHandler(this.btnViewAllUser_Click);
            // 
            // grbFind
            // 
            this.grbFind.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.grbFind.Controls.Add(this.btnFind);
            this.grbFind.Controls.Add(this.tbFind);
            this.grbFind.Controls.Add(this.cbbFind);
            this.grbFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbFind.Location = new System.Drawing.Point(15, 40);
            this.grbFind.Name = "grbFind";
            this.grbFind.Size = new System.Drawing.Size(672, 101);
            this.grbFind.TabIndex = 8;
            this.grbFind.TabStop = false;
            this.grbFind.Text = "Tìm kiếm nhóm";
            // 
            // sqlConnection
            // 
            this.sqlConnection.ConnectionString = "Data Source=DESKTOP-1SC8KP7\\SQLEXPRESS;Initial Catalog=QLTV;Persist Security Info" +
    "=True;User ID=nqt123;Password=nqt123";
            this.sqlConnection.FireInfoMessageEventOnUserErrors = false;
            // 
            // GroupUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1044, 576);
            this.Controls.Add(this.grbInfo);
            this.Controls.Add(this.grbUser);
            this.Controls.Add(this.grbDetail);
            this.Controls.Add(this.grbCRUD);
            this.Controls.Add(this.grbFind);
            this.Name = "GroupUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GroupUser";
            this.grbInfo.ResumeLayout(false);
            this.grbInfo.PerformLayout();
            this.grbUser.ResumeLayout(false);
            this.grbDetail.ResumeLayout(false);
            this.grbDetail.PerformLayout();
            this.grbCRUD.ResumeLayout(false);
            this.grbCRUD.PerformLayout();
            this.grbFind.ResumeLayout(false);
            this.grbFind.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lbCount;
        private System.Windows.Forms.Label lbGrName;
        private System.Windows.Forms.Label lbId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.GroupBox grbInfo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox grbUser;
        private System.Windows.Forms.ListView listView;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lbRoleCount;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox grbDetail;
        private System.Windows.Forms.Label lbAllUser;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.TextBox tbFind;
        private System.Windows.Forms.ComboBox cbbFind;
        private System.Windows.Forms.GroupBox grbCRUD;
        private System.Windows.Forms.Button btnViewAllUser;
        private System.Windows.Forms.GroupBox grbFind;
        private System.Data.SqlClient.SqlConnection sqlConnection;
    }
}