﻿using Quanlythuvien2.DataAccess;
using Quanlythuvien2.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UserAndUserGroup.DTO;

namespace Quanlythuvien2.GUI
{
    public partial class GroupUser : Form
    {
        List<DTO_Role> listRole;
        public GroupUser()
        {
            InitializeComponent();
            LoadUserView();
            RoleCount();
            LoadData();
        }
        public void RoleCount()
        {
            DAL_Role dalRole = new DAL_Role();
            lbRoleCount.Text = dalRole.AllRoleCount().ToString();
        }
        private void btnViewAllUser_Click(object sender, EventArgs e)
        {
            LoadData();
        }
        public void LoadData()
        {
            LoadUserView();
            DAL_Role dalRole = new DAL_Role();
            listRole = dalRole.GetAllRole();
            foreach (DTO_Role role in listRole)
            {
                listView.Items.Add(new ListViewItem(new[] { role.id.ToString(), role.name, role.note }));
            }
        }
        public void LoadUserView()
        {
            RoleCount();
            listView.Clear();
            listView.View = View.Details;
            listView.Columns.Add("ID");
            listView.Columns.Add("Tên nhóm");
            listView.Columns.Add("Ghi chú");
            listView.Columns[0].Width = 50;
            listView.Columns[1].Width = (listView.Width - 50) / 2;
            listView.Columns[2].Width = (listView.Width - 50) / 2;
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            string find = "";
            List<DTO_Role> listRole = new List<DTO_Role>();
            if (String.IsNullOrEmpty(tbFind.Text))
            {
                MessageBox.Show("Hãy nhập thông tin tìm kiếm");
            }
            else
            {
                find = tbFind.Text;
                DAL_Role dalRole = new DAL_Role();
                LoadUserView();
                if (cbbFind.SelectedIndex == 0)
                {
                    listRole = dalRole.FindByName(find);
                }
                if (cbbFind.SelectedIndex == 1)
                {
                    int number;
                    bool result = Int32.TryParse(find, out number);
                    if (result)
                    {
                        listRole = dalRole.FindByID(Convert.ToInt32(find));
                    }
                    else
                    {
                        MessageBox.Show("Từ khoá nhập không hợp lệ, vui lòng nhập chữ số");
                    }
                }
                foreach(DTO_Role role in listRole)
            {
                listView.Items.Add(new ListViewItem(new[] { role.id.ToString(), role.name, role.note }));
            }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddUserGroup addUserGroup = new AddUserGroup();
            addUserGroup.ShowDialog();
            LoadData();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                DAL_Role dalRole = new DAL_Role();
                string id = listView.SelectedItems[0].SubItems[0].Text;
                listRole = dalRole.GetAllRole();
                DTO_Role user = listRole.Find(x => x.id == Convert.ToInt32(id));
                UpdateRole updateRole = new UpdateRole(user);
                updateRole.ShowDialog();
                LoadData();
            }
            catch
            {
                MessageBox.Show("Hãy chọn một vai trò để sửa", "Thông báo");
            }


        }

        private void listView_MouseClick(object sender, MouseEventArgs e)
        {
            DAL_RoleMap dalRoleMap = new DAL_RoleMap();
            string id = listView.SelectedItems[0].SubItems[0].Text;
            DTO_Role role = listRole.Find(x => x.id == Convert.ToInt32(id));
            lbId.Text = id;
            lbGrName.Text = role.name;
            lbCount.Text = dalRoleMap.Count(role).ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result= MessageBox.Show("Bạn có chắc chắn muốn xoá?", "Thông báo xoá", MessageBoxButtons.OKCancel);
            if (result==DialogResult.OK)
            {
                try
                {
                    string id = listView.SelectedItems[0].SubItems[0].Text;
                    DTO_Role role = listRole.Find(x => x.id == Convert.ToInt32(id));
                    DAL_Role dalRole = new DAL_Role();
                    if (dalRole.DeleteRole(role.id))
                    {
                        MessageBox.Show("Xoá thành công");
                        LoadData();
                    }
                }

                catch
                {
                    MessageBox.Show("Hãy chọn nhóm thao tác", "Thông báo");
                }

            }
        }

        private void listView_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }
    }
}
