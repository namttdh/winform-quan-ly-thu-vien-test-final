﻿namespace Quanlythuvien2
{
    partial class OrderDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbListUser = new System.Windows.Forms.ComboBox();
            this.btnFind = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFindUser = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listBookLeft = new System.Windows.Forms.DataGridView();
            this.cbFindBook = new System.Windows.Forms.ComboBox();
            this.btnFindBook = new System.Windows.Forms.Button();
            this.txtBookFind = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnOrder = new System.Windows.Forms.Button();
            this.datePromise = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.listBookRight = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lbUserAddress = new System.Windows.Forms.Label();
            this.lbUserName = new System.Windows.Forms.Label();
            this.lbUserID = new System.Windows.Forms.Label();
            this.lbNameUser = new System.Windows.Forms.Label();
            this.btnAddBook = new System.Windows.Forms.Button();
            this.btnRemoveBook = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBookLeft)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBookRight)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbListUser);
            this.groupBox1.Controls.Add(this.btnFind);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtFindUser);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 163);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Người mượn";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Chọn người mượn";
            // 
            // cbListUser
            // 
            this.cbListUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbListUser.FormattingEnabled = true;
            this.cbListUser.Location = new System.Drawing.Point(6, 123);
            this.cbListUser.Name = "cbListUser";
            this.cbListUser.Size = new System.Drawing.Size(187, 24);
            this.cbListUser.TabIndex = 3;
            this.cbListUser.SelectedIndexChanged += new System.EventHandler(this.cbListUser_SelectedIndexChanged);
            // 
            // btnFind
            // 
            this.btnFind.Location = new System.Drawing.Point(119, 71);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(75, 23);
            this.btnFind.TabIndex = 2;
            this.btnFind.Text = "Tìm";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tìm tên người mượn";
            // 
            // txtFindUser
            // 
            this.txtFindUser.Location = new System.Drawing.Point(7, 43);
            this.txtFindUser.Name = "txtFindUser";
            this.txtFindUser.Size = new System.Drawing.Size(187, 22);
            this.txtFindUser.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.listBookLeft);
            this.groupBox2.Controls.Add(this.cbFindBook);
            this.groupBox2.Controls.Add(this.btnFindBook);
            this.groupBox2.Controls.Add(this.txtBookFind);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(219, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(371, 547);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Sách mượn";
            // 
            // listBookLeft
            // 
            this.listBookLeft.AllowUserToAddRows = false;
            this.listBookLeft.AllowUserToDeleteRows = false;
            this.listBookLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBookLeft.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.listBookLeft.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listBookLeft.Location = new System.Drawing.Point(6, 111);
            this.listBookLeft.Name = "listBookLeft";
            this.listBookLeft.ReadOnly = true;
            this.listBookLeft.RowHeadersVisible = false;
            this.listBookLeft.RowTemplate.Height = 24;
            this.listBookLeft.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.listBookLeft.Size = new System.Drawing.Size(359, 430);
            this.listBookLeft.TabIndex = 4;
            this.listBookLeft.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.listBookLeft_RowsAdded);
            this.listBookLeft.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.listBookLeft_RowsRemoved);
            // 
            // cbFindBook
            // 
            this.cbFindBook.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFindBook.FormattingEnabled = true;
            this.cbFindBook.Items.AddRange(new object[] {
            "Tên sách",
            "ID Sách"});
            this.cbFindBook.Location = new System.Drawing.Point(6, 43);
            this.cbFindBook.Name = "cbFindBook";
            this.cbFindBook.Size = new System.Drawing.Size(121, 24);
            this.cbFindBook.TabIndex = 3;
            // 
            // btnFindBook
            // 
            this.btnFindBook.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFindBook.Location = new System.Drawing.Point(290, 71);
            this.btnFindBook.Name = "btnFindBook";
            this.btnFindBook.Size = new System.Drawing.Size(75, 23);
            this.btnFindBook.TabIndex = 2;
            this.btnFindBook.Text = "Tìm";
            this.btnFindBook.UseVisualStyleBackColor = true;
            this.btnFindBook.Click += new System.EventHandler(this.btnFindBook_Click);
            // 
            // txtBookFind
            // 
            this.txtBookFind.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBookFind.Location = new System.Drawing.Point(129, 43);
            this.txtBookFind.Name = "txtBookFind";
            this.txtBookFind.Size = new System.Drawing.Size(236, 22);
            this.txtBookFind.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Tìm sách";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.btnOrder);
            this.groupBox3.Controls.Add(this.datePromise);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.listBookRight);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Location = new System.Drawing.Point(658, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(370, 547);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Thông tin chi tiết";
            // 
            // btnOrder
            // 
            this.btnOrder.Location = new System.Drawing.Point(222, 15);
            this.btnOrder.Name = "btnOrder";
            this.btnOrder.Size = new System.Drawing.Size(142, 93);
            this.btnOrder.TabIndex = 5;
            this.btnOrder.Text = "Cho mượn";
            this.btnOrder.UseVisualStyleBackColor = true;
            this.btnOrder.Click += new System.EventHandler(this.btnOrder_Click);
            // 
            // datePromise
            // 
            this.datePromise.Location = new System.Drawing.Point(6, 48);
            this.datePromise.MinDate = new System.DateTime(2018, 12, 23, 0, 0, 0, 0);
            this.datePromise.Name = "datePromise";
            this.datePromise.Size = new System.Drawing.Size(200, 22);
            this.datePromise.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 17);
            this.label9.TabIndex = 3;
            this.label9.Text = "Ngày hẹn trả";
            // 
            // listBookRight
            // 
            this.listBookRight.AllowUserToAddRows = false;
            this.listBookRight.AllowUserToDeleteRows = false;
            this.listBookRight.AllowUserToOrderColumns = true;
            this.listBookRight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBookRight.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.listBookRight.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listBookRight.Location = new System.Drawing.Point(9, 111);
            this.listBookRight.Name = "listBookRight";
            this.listBookRight.ReadOnly = true;
            this.listBookRight.RowHeadersVisible = false;
            this.listBookRight.RowTemplate.Height = 24;
            this.listBookRight.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.listBookRight.Size = new System.Drawing.Size(355, 430);
            this.listBookRight.TabIndex = 1;
            this.listBookRight.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.listBookRight_RowsAdded);
            this.listBookRight.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.listBookRight_RowsRemoved);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 91);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(149, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "Tổng số sách mượn: 0";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox4.Controls.Add(this.lbUserAddress);
            this.groupBox4.Controls.Add(this.lbUserName);
            this.groupBox4.Controls.Add(this.lbUserID);
            this.groupBox4.Controls.Add(this.lbNameUser);
            this.groupBox4.Location = new System.Drawing.Point(13, 183);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(200, 377);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Thông tin người mượn";
            // 
            // lbUserAddress
            // 
            this.lbUserAddress.AutoSize = true;
            this.lbUserAddress.Location = new System.Drawing.Point(3, 122);
            this.lbUserAddress.MaximumSize = new System.Drawing.Size(200, 0);
            this.lbUserAddress.Name = "lbUserAddress";
            this.lbUserAddress.Size = new System.Drawing.Size(60, 17);
            this.lbUserAddress.TabIndex = 0;
            this.lbUserAddress.Text = "Address";
            // 
            // lbUserName
            // 
            this.lbUserName.AutoSize = true;
            this.lbUserName.Location = new System.Drawing.Point(3, 50);
            this.lbUserName.MaximumSize = new System.Drawing.Size(200, 0);
            this.lbUserName.Name = "lbUserName";
            this.lbUserName.Size = new System.Drawing.Size(71, 17);
            this.lbUserName.TabIndex = 0;
            this.lbUserName.Text = "username";
            // 
            // lbUserID
            // 
            this.lbUserID.AutoSize = true;
            this.lbUserID.Location = new System.Drawing.Point(3, 28);
            this.lbUserID.MaximumSize = new System.Drawing.Size(200, 0);
            this.lbUserID.Name = "lbUserID";
            this.lbUserID.Size = new System.Drawing.Size(19, 17);
            this.lbUserID.TabIndex = 0;
            this.lbUserID.Text = "id";
            // 
            // lbNameUser
            // 
            this.lbNameUser.AutoSize = true;
            this.lbNameUser.Location = new System.Drawing.Point(3, 74);
            this.lbNameUser.MaximumSize = new System.Drawing.Size(200, 0);
            this.lbNameUser.Name = "lbNameUser";
            this.lbNameUser.Size = new System.Drawing.Size(33, 17);
            this.lbNameUser.TabIndex = 0;
            this.lbNameUser.Text = "Tên";
            // 
            // btnAddBook
            // 
            this.btnAddBook.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddBook.Enabled = false;
            this.btnAddBook.Location = new System.Drawing.Point(596, 267);
            this.btnAddBook.Name = "btnAddBook";
            this.btnAddBook.Size = new System.Drawing.Size(55, 23);
            this.btnAddBook.TabIndex = 3;
            this.btnAddBook.Text = ">>";
            this.btnAddBook.UseVisualStyleBackColor = true;
            this.btnAddBook.Click += new System.EventHandler(this.btnAddBook_Click);
            // 
            // btnRemoveBook
            // 
            this.btnRemoveBook.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemoveBook.Enabled = false;
            this.btnRemoveBook.Location = new System.Drawing.Point(597, 296);
            this.btnRemoveBook.Name = "btnRemoveBook";
            this.btnRemoveBook.Size = new System.Drawing.Size(55, 23);
            this.btnRemoveBook.TabIndex = 3;
            this.btnRemoveBook.Text = "<<";
            this.btnRemoveBook.UseVisualStyleBackColor = true;
            this.btnRemoveBook.Click += new System.EventHandler(this.btnRemoveBook_Click);
            // 
            // OrderDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1040, 572);
            this.Controls.Add(this.btnRemoveBook);
            this.Controls.Add(this.btnAddBook);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "OrderDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chi tiết đơn sách";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OrderDetails_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBookLeft)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBookRight)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbListUser;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFindUser;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lbNameUser;
        private System.Windows.Forms.Label lbUserAddress;
        private System.Windows.Forms.Label lbUserName;
        private System.Windows.Forms.Label lbUserID;
        private System.Windows.Forms.DataGridView listBookLeft;
        private System.Windows.Forms.ComboBox cbFindBook;
        private System.Windows.Forms.Button btnFindBook;
        private System.Windows.Forms.TextBox txtBookFind;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnAddBook;
        private System.Windows.Forms.Button btnRemoveBook;
        private System.Windows.Forms.Button btnOrder;
        private System.Windows.Forms.DateTimePicker datePromise;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView listBookRight;
        private System.Windows.Forms.Label label8;
    }
}