﻿using LibraryManager.DataAccess;
using LibraryManager.DTO;
using LibraryManager.DTO.Opject;
using Quanlythuvien.DataAccess;
using Quanlythuvien2.Core;
using Quanlythuvien2.DataAccess;
using Quanlythuvien2.DTO;
using Quanlythuvien2.Object;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UserAndUserGroup.DTO;

namespace Quanlythuvien2
{
    public partial class OrderDetails : Form
    {
        //public FormBorrow parentForm;
        DAL_User dalUser = new DAL_User();
        DAL_Book dalBook = new DAL_Book();
        DAL_BookOrder dalBookOrder = new DAL_BookOrder();
        List<DTO_User> listUserFind;
        List<int> listIdAdded = new List<int>();
        BookOrderWithName editBookOrder = null;
        List<int> listOldItems = new List<int>();
        public OrderDetails(int idBookOrder = -1)
        {
            InitializeComponent();
            lbNameUser.Text = "";
            lbUserID.Text = "";
            lbUserName.Text = "";
            lbUserAddress.Text = "";
            createColumnsListBook(listBookLeft);
            createColumnsListBook(listBookRight);
            changeToEdit(idBookOrder);
            loadToLeft();
            datePromise.MinDate = DateTime.Now;
            cbFindBook.SelectedIndex = 0;
            


        }

        private void changeToEdit(int idBookOrder)
        {
            if(idBookOrder > 0)
            {
                btnOrder.Text = "Sửa";
                editBookOrder = dalBookOrder.findById(idBookOrder.ToString())[0];
                listUserFind = dalUser.FindByID(editBookOrder.idUser);
                Dictionary<int, String> comboSource = new Dictionary<int, String>();
                comboSource.Add(editBookOrder.idUser, editBookOrder.name);
                cbListUser.DataSource = new BindingSource(comboSource, null);
                cbListUser.DisplayMember = "Value";
                cbListUser.ValueMember = "Key";
                datePromise.Value = Helper.getDateTimeFromUnixTimeStamp(editBookOrder.datePromise);
                loadToRight();
            }

        }

        void createColumnsListBook(DataGridView dataGridView)
        {
            dataGridView.Columns.Add("id", "ID");
            dataGridView.Columns["id"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridView.Columns.Add("name", "Tên sách");
            dataGridView.Columns.Add("nameAuthor", "Tên tác giả");
        }

        private void OrderDetails_FormClosed(object sender, FormClosedEventArgs e)
        {
            //parentForm.Visible = true;
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            cbListUser.DataSource = null;
            if(txtFindUser.Text == "")
            {
                MessageBox.Show("Nhập tên cần tìm");
            }
            else
            {
                listUserFind = dalUser.FindByName(txtFindUser.Text);
                if(listUserFind.Count < 1)
                {
                    MessageBox.Show("Không tìm thấy người dùng");
                }
                else
                {
                    Dictionary<int, String> comboSource = new Dictionary<int, String>();
                    foreach (DTO_User user in listUserFind)
                    {                        
                        comboSource.Add(user.id, user.name);
                    }
                    cbListUser.DataSource = new BindingSource(comboSource, null);
                    cbListUser.DisplayMember = "Value";
                    cbListUser.ValueMember = "Key";
                }
            }
        }

        private void btnOrder_Click(object sender, EventArgs e)
        {
            bool flag = false;
            if (cbListUser.SelectedIndex < 0)
            {
                MessageBox.Show("Vui lòng chọn tên người thuê sách");
            }else if(listIdAdded.Count <= 0)
            {
                MessageBox.Show("Vui lòng chọn ít nhất 1 quyền sách");
            }else if(datePromise.Value.ToString("dd/MM/yyyy").Equals(DateTime.Now.ToString("dd/MM/yyyy")))
            {
                DialogResult dialogResult = MessageBox.Show("Bạn có chắc người mượn sẽ trả trong ngày hôm nay? ", "Ngày hứa trả", MessageBoxButtons.YesNo);
                flag = (dialogResult == DialogResult.Yes);
            }else
            {
                flag = true;
            }
            if (flag)
            {
                DTO_BookOrder bookOrder = new DTO_BookOrder();
                bookOrder.idUser = ((KeyValuePair<int, String>)cbListUser.SelectedItem).Key;                
                bookOrder.datePromise = Helper.ConvertToUnixTime(datePromise.Value);
                if (editBookOrder == null)
                {
                    bookOrder.date = Helper.ConvertToUnixTime(DateTime.Now);
                    dalBookOrder.insertOrderMap(dalBookOrder.insertOrder(bookOrder, listIdAdded.Count), listIdAdded);
                    listBookRight.Rows.Clear();
                    listIdAdded.Clear();
                    loadToLeft();
                    MessageBox.Show("Cho mượn sách thành công");
                }
                else
                {
                    
                    List<int> listAddBook = listIdAdded.Except(listOldItems).ToList<int>();
                    List<int> listRemove = listOldItems.Except(listIdAdded).ToList<int>();
                    bookOrder.date = editBookOrder.date;
                    bookOrder.id = editBookOrder.id;
                    if(listRemove.Count > 0 ) dalBookOrder.deleteOrderMap(editBookOrder.id, listRemove);
                    if(listAddBook.Count > 0 ) dalBookOrder.insertOrderMap(editBookOrder.id, listAddBook);
                    dalBookOrder.updateOrder(bookOrder, listIdAdded.Count);
                    listOldItems.AddRange(listAddBook);
                    listOldItems = listOldItems.Except(listRemove).ToList<int>();
                    MessageBox.Show("Sửa đơn sách thành công");
                }
                    
            }
        }

        private void cbListUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbListUser.SelectedIndex != -1)
            {
                int key = ((KeyValuePair<int, String>)cbListUser.SelectedItem).Key;
                DTO_User user = listUserFind.Find(x => x.id == key);
                lbNameUser.Text = string.Format("Họ tên: \r\n{0}", user.name); 
                lbUserID.Text = string.Format("ID: {0}", user.id); 
                lbUserName.Text = string.Format("Username: {0}", user.username);
                lbUserAddress.Text = string.Format("Địa chỉ: \r\n{0}", user.address);
            }            
        }

        private void btnAddBook_Click(object sender, EventArgs e)
        {
            exChangeRows(listBookLeft, listBookRight, true);
        }
        private void btnRemoveBook_Click(object sender, EventArgs e)
        {
            exChangeRows(listBookRight, listBookLeft, false);
        }
        void exChangeRows(DataGridView from, DataGridView to, bool add = true)
        {
            int selectedRowCount = from.SelectedRows.Count;
            if (from.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow row in from.SelectedRows)
                {
                    if (add)
                    {
                        listIdAdded.Add(Convert.ToInt32(row.Cells[0].Value));
                    }
                    else
                    {
                        listIdAdded.Remove(Convert.ToInt32(row.Cells[0].Value));
                    }
                    to.Rows.Add(row.Cells[0].Value, row.Cells[1].Value, row.Cells[2].Value);
                }
                for (int i = 0; i < selectedRowCount; i++)
                {
                    from.Rows.RemoveAt(from.SelectedRows[0].Index);
                }
            }
        }

        private void listBookLeft_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (listBookLeft.RowCount > 0) btnAddBook.Enabled = true;
        }

        private void listBookLeft_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (listBookLeft.RowCount <= 0) btnAddBook.Enabled = false;
        }

        private void listBookRight_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (listBookRight.RowCount > 0) btnRemoveBook.Enabled = true;
        }

        private void listBookRight_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (listBookRight.RowCount <= 0) btnRemoveBook.Enabled = false;
        }
        private void loadToLeft(string dk = "ORDER BY id DESC")
        {
            listBookLeft.Rows.Clear();
            List<BookTypeMap> bookTypeMaps = dalBook.getAllBook(dk);
            foreach(BookTypeMap book in bookTypeMaps)
            {
                if(!listIdAdded.Contains(book.id)) listBookLeft.Rows.Add(book.id, book.name, book.nameAuthor);
            }
        }
        private void loadToRight()
        {
            listBookRight.Rows.Clear();
            List<DTO_Book> bookTypeMaps = dalBook.findBookByIdOrder(editBookOrder.id.ToString());
            
            foreach (DTO_Book book in bookTypeMaps)
            {
                listIdAdded.Add(book.id);
                listOldItems.Add(book.id);
                listBookRight.Rows.Add(book.id, book.name, book.nameAuthor);
            }
        }

        private void btnFindBook_Click(object sender, EventArgs e)
        {
            if(txtBookFind.Text != "")
            {
                if (cbFindBook.SelectedIndex == 0)
                {
                    loadToLeft(" WHERE bk.name like N'%" + txtBookFind.Text + "%' ");
                }
                else if (cbFindBook.SelectedIndex == 1)
                {
                    if(int.TryParse(txtBookFind.Text,out int n))
                    {
                        loadToLeft(" WHERE bk.id = " + txtBookFind.Text);
                    }
                    else
                    {
                        MessageBox.Show("ID không đúng định dạng");
                    }
                    
                }
            }
            else
            {
                //MessageBox.Show("Bạn chưa nhập thông tin cần tìm");
                loadToLeft();
            }
            
        }
    }
}
