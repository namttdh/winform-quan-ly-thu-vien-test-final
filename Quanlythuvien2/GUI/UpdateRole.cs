﻿using Quanlythuvien2.DataAccess;
using Quanlythuvien2.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quanlythuvien2.GUI
{
    public partial class UpdateRole : Form
    {
        public DTO_Role Role;
        public UpdateRole(DTO_Role _Role)
        {
            InitializeComponent();
            Role = _Role;
            textName.Text = _Role.name;
            textNote.Text = _Role.note;
            Role.id = _Role.id;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void buttonAddGr_Click(object sender, EventArgs e)
        {
            DAL_Role dalRole = new DAL_Role();
            DTO_Role role = new DTO_Role
            {
                name = textName.Text,
                note = textNote.Text,
                id=Role.id
            };
            if (dalRole.UpdateRole(role))
            {
                MessageBox.Show("Sửa thành công", "Thông báo");
            }
            else
            {
                MessageBox.Show("Sửa thất bại, kiểm tra tên vai trò", "Thông báo khẩn cấp");
            }
        }
    }
}
