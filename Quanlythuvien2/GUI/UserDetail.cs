﻿using Quanlythuvien.DataAccess;
using Quanlythuvien2.DataAccess;
using Quanlythuvien2.DTO;
using Quanlythuvien2.GUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UserAndUserGroup.DTO;

namespace Quanlythuvien2.GUI
{
    public partial class UserDetail : Form
    {
        public DTO_User user;
        List<DTO_Role> listRole;
        public UserDetail(DTO_User newUser)
        {
            user = newUser;
            InitializeComponent();
            SetUser();
            listBoxData();
            SelectedRole();
        }
        public void SelectedRole()
        {
            DAL_RoleMap roleMap = new DAL_RoleMap();
            DAL_Role dalRole = new DAL_Role();
            List<int> n = roleMap.GetRoleList(user.id);
            for(int i = 0; i< listBox.Items.Count; i++)
            {
                foreach(int k in n)
                {
                    DTO_Role role = dalRole.GetRole(k);
                    if (listBox.Items[i].ToString() == role.name)
                    {
                        listBox.SelectedIndex = i;
                    }
                }
            }
        }
        public void listBoxData()
        {
            DAL_Role dalRole = new DAL_Role();
            listRole = dalRole.GetAllRole();
            foreach (DTO_Role role in listRole)
            {
                listBox.Items.Add(role.name);
            }
        }
        public void AddOrUpdate()
        {
        }
        public void SetUser()
        {
            textName.Text = this.user.name;
            textUsername.Text = this.user.username;
            //textPassword.Text = this.user.password;
            textAddress.Text = this.user.address;
        }
        
        private void btnSave_Click(object sender, EventArgs e)
        {
            DAL_User dalUser = new DAL_User();
            DTO_User user1 = new DTO_User()
            {
                id=user.id,
                username = textUsername.Text,
                name = textName.Text,
                address = textAddress.Text
            };

            
            if(textPassword.Text != "")
            {
                user1.password = textPassword.Text;
            }
            DAL_Role dalRole = new DAL_Role();
            DAL_RoleMap dalRoleMap = new DAL_RoleMap();
            if (textUsername.Text != user.username && dalUser.UsernameCheck(textUsername.Text))
            {
                MessageBox.Show("Username đã tồn tại");
            }else if (dalUser.Update(user1))
            {
                if (dalRoleMap.DeleteRoleMap(user1.id))
                {
                    DTO_User newUser = dalUser.FindByUsername(user1.username);
                    for (int i = 0; i < listBox.Items.Count; i++)
                    {
                        if (listBox.GetSelected(i) == true)
                        {
                            DTO_RoleMap roleMap = new DTO_RoleMap()
                            {
                                idUser = newUser.id,
                                idRole = listRole[i].id
                            };
                            dalRoleMap.AddRoleMap(roleMap);
                        }
                    }
                    this.Close();
                }
                MessageBox.Show("Sửa thành công");
            }
            else
            {
                MessageBox.Show("Sửa thất bại");
            }
            
        }

        private void lbPassword_Click(object sender, EventArgs e)
        {

        }
    }
}
