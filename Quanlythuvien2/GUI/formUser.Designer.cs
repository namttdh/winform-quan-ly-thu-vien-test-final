﻿namespace UserAndUserGroup
{
    partial class formUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sqlConnection = new System.Data.SqlClient.SqlConnection();
            this.listView = new System.Windows.Forms.ListView();
            this.btnViewAllUser = new System.Windows.Forms.Button();
            this.grbFind = new System.Windows.Forms.GroupBox();
            this.btnFind = new System.Windows.Forms.Button();
            this.tbFind = new System.Windows.Forms.TextBox();
            this.cbbFind = new System.Windows.Forms.ComboBox();
            this.grbDetail = new System.Windows.Forms.GroupBox();
            this.lbUserCount = new System.Windows.Forms.Label();
            this.lbAllUser = new System.Windows.Forms.Label();
            this.grbUser = new System.Windows.Forms.GroupBox();
            this.grbCRUD = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.grbInfo = new System.Windows.Forms.GroupBox();
            this.lbRole = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbAdd = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.lbUsername = new System.Windows.Forms.Label();
            this.lbId = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.grbFind.SuspendLayout();
            this.grbDetail.SuspendLayout();
            this.grbUser.SuspendLayout();
            this.grbCRUD.SuspendLayout();
            this.grbInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // sqlConnection
            // 
            this.sqlConnection.ConnectionString = "Data Source=DESKTOP-1SC8KP7\\SQLEXPRESS;Initial Catalog=QLTV;Persist Security Info" +
    "=True;User ID=nqt123;Password=nqt123";
            this.sqlConnection.FireInfoMessageEventOnUserErrors = false;
            // 
            // listView
            // 
            this.listView.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.listView.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView.FullRowSelect = true;
            this.listView.HideSelection = false;
            this.listView.Location = new System.Drawing.Point(7, 21);
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(641, 365);
            this.listView.TabIndex = 0;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.SelectedIndexChanged += new System.EventHandler(this.listView_SelectedIndexChanged);
            this.listView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listView_MouseClick);
            this.listView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listView_MouseDoubleClick);
            // 
            // btnViewAllUser
            // 
            this.btnViewAllUser.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnViewAllUser.AutoSize = true;
            this.btnViewAllUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewAllUser.Location = new System.Drawing.Point(170, 100);
            this.btnViewAllUser.Name = "btnViewAllUser";
            this.btnViewAllUser.Size = new System.Drawing.Size(160, 61);
            this.btnViewAllUser.TabIndex = 1;
            this.btnViewAllUser.Text = "Tất cả người dùng";
            this.btnViewAllUser.UseVisualStyleBackColor = true;
            this.btnViewAllUser.Click += new System.EventHandler(this.btnViewAll_Click);
            // 
            // grbFind
            // 
            this.grbFind.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.grbFind.Controls.Add(this.btnFind);
            this.grbFind.Controls.Add(this.tbFind);
            this.grbFind.Controls.Add(this.cbbFind);
            this.grbFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbFind.Location = new System.Drawing.Point(13, 11);
            this.grbFind.Name = "grbFind";
            this.grbFind.Size = new System.Drawing.Size(672, 101);
            this.grbFind.TabIndex = 2;
            this.grbFind.TabStop = false;
            this.grbFind.Text = "Tìm kiếm người dùng";
            // 
            // btnFind
            // 
            this.btnFind.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFind.Location = new System.Drawing.Point(394, 62);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(75, 28);
            this.btnFind.TabIndex = 2;
            this.btnFind.Text = "Tìm kiếm";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // tbFind
            // 
            this.tbFind.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbFind.Location = new System.Drawing.Point(191, 25);
            this.tbFind.Name = "tbFind";
            this.tbFind.Size = new System.Drawing.Size(457, 24);
            this.tbFind.TabIndex = 1;
            // 
            // cbbFind
            // 
            this.cbbFind.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbbFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbFind.FormattingEnabled = true;
            this.cbbFind.Items.AddRange(new object[] {
            "Tên người dùng",
            "ID",
            "Địa chỉ"});
            this.cbbFind.Location = new System.Drawing.Point(7, 23);
            this.cbbFind.Name = "cbbFind";
            this.cbbFind.Size = new System.Drawing.Size(153, 26);
            this.cbbFind.TabIndex = 0;
            // 
            // grbDetail
            // 
            this.grbDetail.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.grbDetail.Controls.Add(this.lbUserCount);
            this.grbDetail.Controls.Add(this.lbAllUser);
            this.grbDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbDetail.Location = new System.Drawing.Point(692, 11);
            this.grbDetail.Name = "grbDetail";
            this.grbDetail.Size = new System.Drawing.Size(336, 102);
            this.grbDetail.TabIndex = 3;
            this.grbDetail.TabStop = false;
            this.grbDetail.Text = "Thống kê người dùng";
            // 
            // lbUserCount
            // 
            this.lbUserCount.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbUserCount.AutoSize = true;
            this.lbUserCount.Location = new System.Drawing.Point(156, 28);
            this.lbUserCount.Name = "lbUserCount";
            this.lbUserCount.Size = new System.Drawing.Size(35, 18);
            this.lbUserCount.TabIndex = 1;
            this.lbUserCount.Text = "100";
            // 
            // lbAllUser
            // 
            this.lbAllUser.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbAllUser.AutoSize = true;
            this.lbAllUser.Location = new System.Drawing.Point(7, 28);
            this.lbAllUser.Name = "lbAllUser";
            this.lbAllUser.Size = new System.Drawing.Size(143, 18);
            this.lbAllUser.TabIndex = 0;
            this.lbAllUser.Text = "Tổng người dùng: ";
            // 
            // grbUser
            // 
            this.grbUser.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.grbUser.Controls.Add(this.listView);
            this.grbUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbUser.Location = new System.Drawing.Point(13, 115);
            this.grbUser.Name = "grbUser";
            this.grbUser.Size = new System.Drawing.Size(672, 392);
            this.grbUser.TabIndex = 4;
            this.grbUser.TabStop = false;
            this.grbUser.Text = "Danh sách người dùng";
            // 
            // grbCRUD
            // 
            this.grbCRUD.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.grbCRUD.Controls.Add(this.btnDelete);
            this.grbCRUD.Controls.Add(this.btnViewAllUser);
            this.grbCRUD.Controls.Add(this.btnUpdate);
            this.grbCRUD.Controls.Add(this.btnAdd);
            this.grbCRUD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbCRUD.Location = new System.Drawing.Point(691, 119);
            this.grbCRUD.Name = "grbCRUD";
            this.grbCRUD.Size = new System.Drawing.Size(336, 175);
            this.grbCRUD.TabIndex = 6;
            this.grbCRUD.TabStop = false;
            this.grbCRUD.Text = "Quản lý người dùng";
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(10, 100);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(160, 61);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "Xoá";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(170, 33);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(160, 61);
            this.btnUpdate.TabIndex = 1;
            this.btnUpdate.Text = "Sửa";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(10, 33);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(160, 61);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // grbInfo
            // 
            this.grbInfo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.grbInfo.Controls.Add(this.lbRole);
            this.grbInfo.Controls.Add(this.label1);
            this.grbInfo.Controls.Add(this.lbAdd);
            this.grbInfo.Controls.Add(this.lbName);
            this.grbInfo.Controls.Add(this.lbUsername);
            this.grbInfo.Controls.Add(this.lbId);
            this.grbInfo.Controls.Add(this.label5);
            this.grbInfo.Controls.Add(this.label3);
            this.grbInfo.Controls.Add(this.label2);
            this.grbInfo.Controls.Add(this.label);
            this.grbInfo.Location = new System.Drawing.Point(691, 301);
            this.grbInfo.Name = "grbInfo";
            this.grbInfo.Size = new System.Drawing.Size(330, 206);
            this.grbInfo.TabIndex = 7;
            this.grbInfo.TabStop = false;
            this.grbInfo.Text = "Thông tin";
            // 
            // lbRole
            // 
            this.lbRole.AutoSize = true;
            this.lbRole.Location = new System.Drawing.Point(132, 136);
            this.lbRole.Name = "lbRole";
            this.lbRole.Size = new System.Drawing.Size(59, 17);
            this.lbRole.TabIndex = 11;
            this.lbRole.Text = "Quản trị";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 136);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Vai trò";
            // 
            // lbAdd
            // 
            this.lbAdd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbAdd.AutoSize = true;
            this.lbAdd.Location = new System.Drawing.Point(130, 105);
            this.lbAdd.Name = "lbAdd";
            this.lbAdd.Size = new System.Drawing.Size(51, 17);
            this.lbAdd.TabIndex = 9;
            this.lbAdd.Text = "Hà Nội";
            // 
            // lbName
            // 
            this.lbName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(129, 78);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(79, 17);
            this.lbName.TabIndex = 7;
            this.lbName.Text = "Quý Thắng";
            // 
            // lbUsername
            // 
            this.lbUsername.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbUsername.AutoSize = true;
            this.lbUsername.Location = new System.Drawing.Point(129, 49);
            this.lbUsername.Name = "lbUsername";
            this.lbUsername.Size = new System.Drawing.Size(52, 17);
            this.lbUsername.TabIndex = 6;
            this.lbUsername.Text = "nqt123";
            // 
            // lbId
            // 
            this.lbId.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbId.AutoSize = true;
            this.lbId.Location = new System.Drawing.Point(130, 22);
            this.lbId.Name = "lbId";
            this.lbId.Size = new System.Drawing.Size(16, 17);
            this.lbId.TabIndex = 5;
            this.lbId.Text = "1";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Địa chỉ";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Họ và tên";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tên đăng nhập";
            // 
            // label
            // 
            this.label.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(7, 22);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(25, 17);
            this.label.TabIndex = 0;
            this.label.Text = "ID:";
            // 
            // formUser
            // 
            this.AcceptButton = this.btnFind;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1040, 572);
            this.Controls.Add(this.grbInfo);
            this.Controls.Add(this.grbCRUD);
            this.Controls.Add(this.grbUser);
            this.Controls.Add(this.grbDetail);
            this.Controls.Add(this.grbFind);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "formUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Người Dùng";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grbFind.ResumeLayout(false);
            this.grbFind.PerformLayout();
            this.grbDetail.ResumeLayout(false);
            this.grbDetail.PerformLayout();
            this.grbUser.ResumeLayout(false);
            this.grbCRUD.ResumeLayout(false);
            this.grbCRUD.PerformLayout();
            this.grbInfo.ResumeLayout(false);
            this.grbInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Data.SqlClient.SqlConnection sqlConnection;
        private System.Windows.Forms.ListView listView;
        private System.Windows.Forms.Button btnViewAllUser;
        private System.Windows.Forms.GroupBox grbFind;
        private System.Windows.Forms.ComboBox cbbFind;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.TextBox tbFind;
        private System.Windows.Forms.GroupBox grbDetail;
        private System.Windows.Forms.Label lbAllUser;
        private System.Windows.Forms.Label lbUserCount;
        private System.Windows.Forms.GroupBox grbUser;
        private System.Windows.Forms.GroupBox grbCRUD;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox grbInfo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label lbAdd;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Label lbUsername;
        private System.Windows.Forms.Label lbId;
        private System.Windows.Forms.Label lbRole;
        private System.Windows.Forms.Label label1;
    }
}

