﻿using Quanlythuvien.DataAccess;
using Quanlythuvien2.DataAccess;
using Quanlythuvien2.GUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UserAndUserGroup.DTO;

namespace UserAndUserGroup
{
    public partial class formUser : Form
    {
        List<DTO_User> listUser;
        List<int> listRole;
        public formUser()
        {
            InitializeComponent();
            loadData();
            cbbFind.SelectedIndex = 0;
        }
        public string listRoleConvert(List<int> n)
        {
            int k = 0;
            DAL_Role role = new DAL_Role();
            String returnString = "";
            foreach(int i in n)
            {
                if( k < n.Count-1)
                 returnString += role.GetRole(i).name + " - ";
                if (k == n.Count - 1)
                    returnString += role.GetRole(i).name;
                k++;
            }
            return returnString;
        }
        private void loadData()
        {
            DAL_RoleMap roleMap = new DAL_RoleMap();
            DAL_User dalUser = new DAL_User();
             listUser = dalUser.GetAllUser();
            LoadUserView();
            foreach (DTO_User user in listUser)
            {
                listRole = roleMap.GetRoleList(user.id);
                listView.Items.Add(new ListViewItem(new[] { user.id.ToString(),user.username.ToString(),
                user.name.ToString(),user.address.ToString(),listRoleConvert(listRole)}));
            }
            //listView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            //listView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }
        private void btnViewAll_Click(object sender, EventArgs e)
        {
            loadData();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            string find="";
            List<DTO_User> listUser=new List<DTO_User>();
            if(String.IsNullOrEmpty(tbFind.Text))
            {
                MessageBox.Show("Hãy nhập thông tin tìm kiếm");
            }
            else
            {
                find = tbFind.Text;
                DAL_User dalUser = new DAL_User();
                LoadUserView();
                if (cbbFind.SelectedIndex == 0)
                {
                    listUser = dalUser.FindByName(find);
                }
                if (cbbFind.SelectedIndex == 1)
                {
                    int number;
                    bool result = Int32.TryParse(find, out number);
                    if (result)
                    {
                        listUser = dalUser.FindByID(Convert.ToInt32(find));
                    }
                    else
                    {
                        MessageBox.Show("Từ khoá nhập không hợp lệ, vui lòng nhập chữ số");
                    }
                }
                if (cbbFind.SelectedIndex == 2)
                {
                    listUser = dalUser.FindByAddress(find);
                }
                foreach (DTO_User user in listUser)
                {
                    listView.Items.Add(new ListViewItem(new[] { user.id.ToString(),user.username.ToString(),
                    user.name.ToString(),user.address.ToString()}));
                }
            }
            //listView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            //listView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }
        public void LoadUserView()
        {
            listView.Clear();
            listView.View = View.Details;
            listView.Columns.Add("ID");
            listView.Columns.Add("Tên đăng nhập");
            listView.Columns.Add("Họ và tên");
            listView.Columns.Add("Địa chỉ");
            listView.Columns.Add("Vai trò");
            listView.Columns[0].Width = 30;
            listView.Columns[1].Width = (listView.Width - 30) / 4;
            listView.Columns[2].Width = (listView.Width - 30) / 5;
            listView.Columns[3].Width = (listView.Width - 30) / 5;
            listView.Columns[4].Width = (listView.Width - 30) / 3;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnDetailUpdate_Click(object sender, EventArgs e)
        {
            DAL_User dalUser = new DAL_User();
            int UserCount=dalUser.AllUserCount();
            lbUserCount.Text = UserCount.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            btnDetailUpdate_Click(sender,e);
        }

        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listView_MouseClick(object sender, MouseEventArgs e)
        {
            string id = listView.SelectedItems[0].SubItems[0].Text;
            DTO_User user = listUser.Find(x => x.id == Convert.ToInt32(id));
            DAL_RoleMap dalRoleMap = new DAL_RoleMap();
            listRole = dalRoleMap.GetRoleList(user.id);
            lbId.Text = id;
            lbUsername.Text = user.username;
            lbName.Text = user.name;
            lbAdd.Text = user.address;
            lbRole.Text = listRoleConvert(listRole);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                string id = listView.SelectedItems[0].SubItems[0].Text;
                DTO_User user = listUser.Find(x => x.id == Convert.ToInt32(id));

                UserDetail userDetail = new UserDetail(user);
                userDetail.AddOrUpdate();
                userDetail.ShowDialog();
                loadData();
            }
            catch
            {
                MessageBox.Show("Hãy chọn một người dùng");
            }
    //listView.
}

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddUser addUser = new AddUser();
            addUser.ShowDialog();
            loadData();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DAL_User dalUser = new DAL_User();
            try
            {
                string id = listView.SelectedItems[0].SubItems[0].Text;
                DTO_User user = listUser.Find(x => x.id == Convert.ToInt32(id));
                DialogResult result = MessageBox.Show("Bạn có muốn xoá? Dữ liệu không thể khôi phục lại", "Xác nhận", MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                {
                    dalUser.DeleteUser(Convert.ToInt32(id));
                    loadData();
                    MessageBox.Show("Xoá Thành Công");
                }
            }
            catch
            {
                MessageBox.Show("Hãy chọn một người dùng", "Thông báo");
            }
        }

        private void listView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                string id = listView.SelectedItems[0].SubItems[0].Text;
                DTO_User user = listUser.Find(x => x.id == Convert.ToInt32(id));

                UserDetail userDetail = new UserDetail(user);
                userDetail.AddOrUpdate();
                userDetail.ShowDialog();
                loadData();
            }
            catch
            {
                
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }
}
