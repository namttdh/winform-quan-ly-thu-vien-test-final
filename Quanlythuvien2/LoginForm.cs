﻿using Quanlythuvien.DataAccess;
using Quanlythuvien2.DataAccess;
using Quanlythuvien2.Object;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UserAndUserGroup.DTO;

namespace Quanlythuvien2
{
    public partial class LoginForm : Form
    {
        DAL_User dalUser = new DAL_User();
        DAL_RoleMap dalRoleMap = new DAL_RoleMap();
        public LoginForm()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if(txtUsername.Text == "" && txtPassword.Text == "" )
            {
                MessageBox.Show("Vui lòng nhập tên đăng nhập hoặc tài khoản");
            }
            UserWithRole user = dalUser.getUserByUsernameAndPass(txtUsername.Text, txtPassword.Text);
            if(user.id == 0)
            {
                MessageBox.Show("Tên đăng nhập hoặc tài khoản không chính xác");
            }
            else
            {
                this.Hide();
                user.role = dalRoleMap.GetRoleList(user.id);
                if (user.role.Contains(1))
                {
                    FormAdmin formAdmin = new FormAdmin();
                    formAdmin.loginForm = this;
                    formAdmin.Show();
                }
                else
                {
                    FormUser formUser = new FormUser();
                    formUser.loginForm = this;
                    formUser.Show();
                }                
            }
            
        }
    }
}
