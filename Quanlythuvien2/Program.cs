﻿using LibraryManager.GUI;
using LibraryManager.GUI.GUI_Book;
using Quanlythuvien2.GUI;
using Quanlythuvien2.GUI.GUI_Book;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using UserAndUserGroup;

namespace Quanlythuvien2
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LoginForm());
        }
    }
}
